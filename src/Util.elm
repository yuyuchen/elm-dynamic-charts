module Util exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Events exposing (onClick)
import Axis
import Path exposing (Path)
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, text_, svg)
import TypedSvg.Attributes exposing (stroke, class, textAnchor, dominantBaseline, transform, viewBox, fill)
import TypedSvg.Attributes.InPx exposing (x, y)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Events exposing (onDblClick)
import TypedSvg.Types exposing (AnchorAlignment(..), DominantBaseline(..), Transform(..), Paint(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)
import Json.Decode as Decode exposing (at)
import List.Extra exposing (uncons)
import Array

-- Prezance packages --
import Update exposing (Msg(..))
import Model exposing (PrzTableCell, ChartSettingsModel, ChartModel, initialTableHeader)



------------------------------------- WaterfallChart -------------------------------------

convertToWaterfalldata : ChartModel -> List (String, String)
convertToWaterfalldata model=
  List.map (\row -> (.name <|  Maybe.withDefault (PrzTableCell "" "") <| Array.get 0 <| Array.fromList row
  ,  .name <|  Maybe.withDefault (PrzTableCell "" "") <| Array.get (model.waterfallChart.colIndex + 1) <| Array.fromList row))
  <| Maybe.withDefault []
  <| List.tail model.data

dataOnly : ChartModel -> List Float
dataOnly model =
  convertToWaterfalldata model |> List.map (\(x,y)-> Maybe.withDefault 0 <| String.toFloat y )

cumulData :  ChartModel -> Int -> List Float ->List Float
cumulData model index dataList =
  let
    myArray = Array.fromList dataList
    updatedArray =
      myArray
      |> Array.set index ( (Array.get index myArray |> Maybe.withDefault 0)
                      + (Array.get (index-1) myArray |> Maybe.withDefault 0)
                      )
      |> Array.toList
  in
    if List.member index model.waterfallChart.soldRows then
      cumulData model (index + 1) dataList
    else if index >= List.length dataList then
      dataList
    else
      cumulData model (index + 1) updatedArray


------------------------------------------ Curve Chart ----------------------------------------

-- format data for draw curve
lineFormatData :  ChartModel-> List (List(String,Float))
lineFormatData model =
  let
    dataHeader =
      List.map .name
      <| Maybe.withDefault []
      <| List.tail
      <| Maybe.withDefault []
      <| List.head model.data

    dataBody = getLineDataBody model
  in
    List.map (\dataB -> List.map2 Tuple.pair dataHeader dataB) dataBody


-- just data value
getLineDataBody : ChartModel -> List (List Float)
getLineDataBody model =
  List.map  (\r -> List.map (\cc -> Maybe.withDefault 0 <| String.toFloat cc.name) r)
  <| List.map (\r -> Maybe.withDefault [] <| List.tail r)
  <| if model.chartsTabs == 7 then
        model.doubleChart.dataInCurve
     else
        Maybe.withDefault []
        <| List.tail model.displayData



-- list of color
getLinesColor : ChartModel -> List String
getLinesColor model =
  List.map  .color
  <| List.map (\r -> Maybe.withDefault (PrzTableCell "" "") <| List.head r)
  <| if model.chartsTabs == 7 then
        model.doubleChart.dataInCurve
     else
        Maybe.withDefault []
        <| List.tail model.displayData


getAbscAndVAlue : ChartModel -> List (List ( Float, ( Float, Float ) ))
getAbscAndVAlue model =
  List.map (\(index, abscListRow) -> List.map2 Tuple.pair (Maybe.withDefault [] <| Array.get index <|Array.fromList <| getLineDataBody model)  abscListRow )
  <| List.indexedMap Tuple.pair
  <| List.map (\r -> List.map (\(str, float) -> Maybe.withDefault (0,0) <| transformToLineData model (str, float ) ) r)
  <| lineFormatData model


transformToLineData : ChartModel -> ( String, Float ) -> Maybe ( Float, Float )
transformToLineData model ( x, y )  =
  let
    data =
       Maybe.withDefault [] <| List.head <| lineFormatData model

  in
    Just ( Scale.convert (xScaleC model data ) x, Scale.convert (yScaleC model) y )


tranfromToAreaData : ChartModel -> ( String, Float ) -> Maybe ( ( Float, Float ), ( Float, Float ) )
tranfromToAreaData model ( x, y ) =
    let
      data =
         Maybe.withDefault [] <| List.head <| lineFormatData model

      sub =  model.height - ( Scale.convert (yScaleC model) 0) - (2 * model.padding.top)
    in
    Just
        ( ( Scale.convert (xScaleC model data ) x, Tuple.first (Scale.rangeExtent (yScaleC model))  - sub)
        , ( Scale.convert (xScaleC model data ) x, Scale.convert (yScaleC model) y )
        )

circle : Path
circle =
    Shape.arc
        { innerRadius = 0
        , outerRadius = 3
        , cornerRadius = 0
        , startAngle = 0
        , endAngle = 2 * pi
        , padAngle = 0
        , padRadius = 0
        }


xScaleC :  ChartModel -> List ( String, Float )->  BandScale String
xScaleC model dataRow  =
    List.map Tuple.first dataRow |>
      Scale.band { defaultBandConfig | paddingInner = 0.75, paddingOuter = 0 }
      ( 0, model.width - (if legendIsMiddle model  then 6 else 2 ) * model.padding.top + ( if legendIsMiddle model  && model.chartsTabs == 7 then -20 else 0)  + (if (model.chartsTabs == 3 || model.chartsTabs == 4) then 50 else 0))


yScaleC : ChartModel -> ContinuousScale Float
yScaleC model =
    Scale.linear ( model.height - 2 * model.padding.top
                  , if legendIsMiddle model  then -50 else 0
                  ) ( model.minHeightScale, model.maxHeightScale )




------------------------------- Bar Chart  ------------------------------------

-- for Bar Chart
convertDataToFloat : String -> ChartSettingsModel -> Float
convertDataToFloat number settings =
  let
    decimalSeparator = settings.decimalSeparator
    thousandsSeparator = settings.thousandsSeparator
    sufix = settings.dataSuffix
    prefix = settings.dataPrefix
    delSeparators =  String.replace prefix "" <| String.replace sufix "" <| String.replace thousandsSeparator ""  number
    val = if decimalSeparator == "," then String.replace decimalSeparator "." delSeparators else delSeparators
  in
    Maybe.withDefault 0 <| String.toFloat val


getAbscissesAndColors : List (List PrzTableCell) -> ( List String, List ( String, List String ) )
getAbscissesAndColors data =
    ( List.map List.Extra.uncons data
        |> List.map (Maybe.withDefault ( initialTableHeader, [] ))
        |> List.map Tuple.first
        |> List.map .color
    , List.Extra.transpose data
        |> List.map List.Extra.uncons
        |> List.map (Maybe.withDefault ( initialTableHeader, [] ))
        |> List.map (\( a, b ) -> ( a.name, List.map .name b ))
        |> List.drop 1
    )


maxNumbFunc : List String -> Float
maxNumbFunc sub_l =
    sub_l
        |> List.map (\v -> Maybe.withDefault 0 (String.toFloat v))
        |> List.maximum
        |> Maybe.withDefault 0


minNumbFunc : List String -> Float
minNumbFunc sub_l =
    sub_l
        |> List.map (\v -> Maybe.withDefault 0 (String.toFloat v))
        |> List.minimum
        |> Maybe.withDefault 0


getMin dataList =
  List.map minNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) dataList)
      |> List.minimum
      |> Maybe.withDefault 0

getMax dataList =
  List.map maxNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) dataList)
      |> List.maximum
      |> Maybe.withDefault 0

yScale : ChartModel -> ContinuousScale Float
yScale model =
    -- Scale.linear ( model.height - 2 * model.padding.top, 0 ) ( 0, model.maxHeightScale )
    let
      maxDoubleBarAxe = getMax model.doubleChart.dataInBar
      minDoubleBarAxe = getMin model.doubleChart.dataInBar

      minH =
        if model.chartsTabs == 0 then
          model.minHeightScale
        else
          minDoubleBarAxe

      maxH =
        if model.chartsTabs == 0 then
          model.maxHeightScale
        else
          maxDoubleBarAxe
    in
    Scale.linear
        ( model.height - 2 * model.padding.top
        , if legendIsMiddle model  then
            -50

          else
            0
        )
        (if   model.minHeightScale >= 0 then 0 else   model.minHeightScale, model.maxHeightScale )


column : ChartModel -> BandScale String -> ( String, List String ) -> Svg Msg
column tmodel bscale ( date, listNW ) =
    let
        sub =  tmodel.height - ( Scale.convert (yScale tmodel) 0) - (2 * tmodel.padding.top)

        funcColumn : ( Int, String ) -> Svg Msg
        funcColumn ( i, str ) =
            let
                (sci, simpleData) = formatData str tmodel.settings

                val =
                  if String.contains "-" str then
                  (Maybe.withDefault 0 <| String.toFloat ("-" ++ String.fromFloat (convertDataToFloat sci tmodel.settings)))
                  else
                    convertDataToFloat sci tmodel.settings

                (defaultVal, simpleVal) =
                  if String.contains "-" str then
                    ("-"++sci, "-"++simpleData)
                  else
                    (sci, simpleData)

                scaleConv =
                    if val < 0 then
                      (Scale.convert (yScale tmodel) 0  + (tmodel.height - (Scale.convert (yScale tmodel) val) - (2 * tmodel.padding.top)))
                    else
                      Scale.convert (yScale tmodel) val

                rectHeight =
                    tmodel.height - scaleConv - (2 * tmodel.padding.top)
                t =
                    Transition.value tmodel.barChart.transition - scaleConv - (2 * tmodel.padding.top)

                xx = Scale.convert bscale date + (Scale.bandwidth bscale * toFloat i) + 2
                yy = if val < 0 then (  Scale.convert (yScale tmodel) 0) else scaleConv


            in
            g [ TypedSvg.Attributes.class [ "column" ++ String.fromInt (i + 1) ], onDblClick <| Stacked (not tmodel.barChart.stacked) ]
                [
                  rect
                    [ x <| xx
                    , y <| yy
                    , TypedSvg.Attributes.InPx.width <| Scale.bandwidth bscale
                    , TypedSvg.Attributes.InPx.height <| rectHeight - (if val < 0 then (0) else sub )
                    ]
                    []
                , text_
                    [ x <| Scale.convert (Scale.toRenderable identity bscale) date + (Scale.bandwidth bscale * toFloat i)
                    , y <| 0
                    -- , y <| scaleConv - 10
                    , textAnchor AnchorMiddle
                    , TypedSvg.Attributes.class [ "data-value" ]
                    ]
                    [ TypedSvg.Core.text <| if tmodel.settings.simpleData && simpleVal /= "" then simpleVal else defaultVal ]
                , g [ TypedSvg.Attributes.class [ "block" ] ]
                    [
                      rect
                        [ x <| Scale.convert bscale date + (Scale.bandwidth bscale * toFloat i) - 1 + 2
                        , y <| (if val < 0 then (  Scale.convert (yScale tmodel) 0) else scaleConv) - 1
                        , TypedSvg.Attributes.InPx.width <| Scale.bandwidth bscale + 2
                        , TypedSvg.Attributes.InPx.height
                            (if t >= 0 then
                                t
                             else
                                0
                            )
                        ]
                        []
                    ]
                ]
    in
    g []
        (List.map funcColumn (List.indexedMap Tuple.pair listNW))




-------------------------------------------- data format functions ---------------------------------------

-- data separators
formatData : String -> ChartSettingsModel ->( String,String)
formatData string settings =
  let
    decimalSeparator = settings.decimalSeparator
    thousandsSeparator = settings.thousandsSeparator

    str = String.replace "-" "" string

    val = String.replace "," "." str
    valLength = String.length val

    getDots =
      if String.contains "." val then
        String.indexes "." val
      else []

    indexDot =
      if List.length getDots > 1 || List.length getDots == 0 then -1
      else String.toInt (String.join "" <| List.map String.fromInt getDots) |> Maybe.withDefault -1

    decimal =
      if indexDot /= -1 then
        String.dropLeft (indexDot+1) val
      else ""

    digits =
      if indexDot /= -1 then
        String.left indexDot val
      else  String.replace "." "" val

    stringArray =
      if String.length digits > 3 then
        List.map (\x-> if remainderBy 3 ((Tuple.first x)) == 0 && (Tuple.first x) /= 0 then
                          (String.fromChar (Tuple.second x))++ thousandsSeparator
                        else
                          String.fromChar (Tuple.second x)
                  )<| List.indexedMap Tuple.pair <| List.reverse <| String.toList digits
      else [digits]

    simpleData =  settings.dataPrefix ++ getSimpleData digits ++ settings.dataSuffix
    data = settings.dataPrefix ++ (String.concat <| List.reverse stringArray) ++ (if decimal /= "" then decimalSeparator ++ decimal else "") ++ settings.dataSuffix
  in
    (data, simpleData)


getSimpleData : String -> String
getSimpleData str =
  let
    digits = String.replace "-" "" str

    findIndex =
      if  4 <=  String.length digits  && String.length digits <= 6 then
        String.concat <| List.reverse <| List.map (\x-> if remainderBy 3 ((Tuple.first x)) == 0 && (Tuple.first x) /= 0 then
                          (String.fromChar (Tuple.second x))++ "."
                        else
                          String.fromChar (Tuple.second x)
                  )<| List.indexedMap Tuple.pair <| List.reverse <| String.toList digits
      else if  7 <=  String.length digits  && String.length digits <= 9 then --Million
        String.concat <| List.reverse <| List.map (\x-> if remainderBy 6 ((Tuple.first x)) == 0 && (Tuple.first x) /= 0 then
                          (String.fromChar (Tuple.second x))++ "."
                        else
                          String.fromChar (Tuple.second x)
                  )<| List.indexedMap Tuple.pair <| List.reverse <| String.toList digits
      else if  10 <=  String.length digits  && String.length digits <= 12 then -- Billion
        String.concat <| List.reverse <| List.map (\x-> if remainderBy 9 ((Tuple.first x)) == 0 && (Tuple.first x) /= 0 then
                          (String.fromChar (Tuple.second x))++ "."
                        else
                          String.fromChar (Tuple.second x)
                  )<| List.indexedMap Tuple.pair <| List.reverse <| String.toList digits
      else if  13 <=  String.length digits  && String.length digits <= 15 then -- Trillon
        String.concat <| List.reverse <| List.map (\x-> if remainderBy 12 ((Tuple.first x)) == 0 && (Tuple.first x) /= 0 then
                          (String.fromChar (Tuple.second x))++ "."
                        else
                          String.fromChar (Tuple.second x)
                  )<| List.indexedMap Tuple.pair <| List.reverse <| String.toList digits
      else ""


    number =  String.left (Maybe.withDefault 0 <|  String.toInt <|String.join "" <| List.map String.fromInt (String.indexes "." findIndex)) findIndex

    decimal =
          String.left 2 <| String.dropLeft (Maybe.withDefault 1 <|  String.toInt <|String.join "" <| List.map String.fromInt (String.indexes "." findIndex)) findIndex

    letter =
      if  4 <=  String.length digits  && String.length digits <= 6 then "K"
      else if  7 <=  String.length digits  && String.length digits <= 9 then "M"
      else if  10 <=  String.length digits  && String.length digits <= 12 then "B"
      else if  13 <=  String.length digits  && String.length digits <= 15 then "T"
      else if String.length digits <=3 then "K"
      else ""

    d =
      if String.length digits == 1 then
        "0.00" ++String.left 1 digits
      else if String.length digits == 2 then
        "0.0" ++String.left 2 digits
      else
        "0." ++String.left 2 digits

    in
      number ++ (if String.contains "0" decimal || String.length number >= 2 then ""
                else if number == "" then d
                else decimal)++ letter




-----------------------------------------  calculate position------------------------------------------

legendIsMiddle : ChartModel -> Bool
legendIsMiddle model =
  String.contains "middle" model.settings.legendPosition


getChartPosition : { width:Float,   height:Float, titlePosition:String, legendPosition:String, sourcePosition:String, chartsTabs:Int} -> (Float, Float)
getChartPosition model =
  let
      sourceTop =
        String.contains "top" model.sourcePosition

      titlesTop =
          String.contains "top" model.titlePosition

      legendTop =
          String.contains "top" model.legendPosition

      legendMiddleLeft =
          model.legendPosition == "middle_left"

      legendMiddleRight =
          model.legendPosition == "middle_right"

      y =
          case ( titlesTop, legendTop ) of
              ( True, False) ->
                  0   + ( if sourceTop then (if model.chartsTabs == 2 && (legendMiddleLeft || legendMiddleRight) then 50 else 0) else 30)
                      + (if legendMiddleLeft || legendMiddleRight then
                          -40

                         else
                          0
                        )

              -- title top
              ( True, True) ->
                  -40 + ( if sourceTop then 0 else 30)

              ( False, False) ->
                  40
                      + (if legendMiddleLeft || legendMiddleRight then
                          -15

                         else
                          20
                        )
                      + ( if sourceTop then -20 else 0)

              -- title bottom
              ( False, True ) ->
                  30 + ( if sourceTop then -30 else 0)

      x =
          case ( titlesTop, legendMiddleLeft, legendMiddleRight ) of
              ( True, True, False ) ->
                  (round model.width // 2 // 2 - 50) * -1  + (if model.chartsTabs == 2 then 100 else 0)
              ( False, True, False ) ->
                if sourceTop then
                  (round model.width // 2 // 2 ) * -1 + (if model.chartsTabs == 2 then 200 else 0)
                else
                  (round model.width // 2 // 2 ) * -1 + 50 + (if model.chartsTabs == 2 then 100 else 0)

              _ ->
                  0
  in
    ( toFloat x, toFloat y )


getLegendPosition : ChartModel -> Int -> (Float, Float)
getLegendPosition model i =
  let
    resW = round model.width // List.length model.data
    resH = round model.height // List.length model.data

    titlesTop = String.contains "top" model.settings.titlePosition
    legendTop =String.contains "top" model.settings.legendPosition
    sourceTop = String.contains "top" model.settings.sourcePosition
    legendMiddle = String.contains "middle" model.settings.legendPosition
    legendMiddleLeft = (model.settings.legendPosition == "middle_left")
    legendMiddleRight = (model.settings.legendPosition == "middle_right")

    addX =
        if String.contains "left" model.settings.legendPosition then
            toFloat (-1 * (resW // 2 + (resW // 2 // 2)))
        else if String.contains "center" model.settings.legendPosition then
            toFloat (-1 * (resW // 2 // 2))
        else
            toFloat (resW // 2 // 2)

    addY =
        case ( titlesTop, legendTop ) of
            ( True, True ) -> if sourceTop then 90 else 70
            ( True, False ) -> if sourceTop then model.height + 60 else model.height + 30
            ( False, True ) -> if sourceTop then 60 else 30
            ( False, False ) -> if sourceTop then model.height + 20 else model.height

    posX =
        if not legendMiddle then
            toFloat (resW * i) + addX
        else if legendMiddleLeft then
            40
        else
            model.width - 150

    posY =
        if not legendMiddle then
            addY
        else if legendMiddle && not titlesTop then
            toFloat (resH // 2 * i + round model.height // 2)
        else
            toFloat (resH // 2 * i + 80)
  in
    ( posX, posY)
