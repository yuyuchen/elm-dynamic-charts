module View exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onMouseUp, onBlur, onInput, onCheck)
import Html.Events.Extra exposing (onChange)
import Axis
import Path exposing (Path)
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, text_, svg, line)
import TypedSvg.Attributes exposing (class, textAnchor, dominantBaseline, transform, viewBox, fill, stroke)
import TypedSvg.Attributes.InPx exposing (x, y, strokeWidth)
import TypedSvg.Events exposing (onLoad)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (AnchorAlignment(..), DominantBaseline(..), Transform(..), Paint(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)
import Json.Decode as Decode exposing (at)
import List.Extra exposing (uncons)
import Array

-- Prezance packages --
import Model exposing (ChartModel)
import Update exposing (Msg(..))
import Charts.BarChart exposing (barChartView, stackedView, config)
import Charts.HorizontalBarChart exposing (horizontalBarChartView, horizontalStackedView, horizontalConfig)
import Charts.PieChart exposing ( pieChartView)
import Charts.CurveChart exposing (curveChartView)
import Charts.AreaChart exposing (areaChartView)
import Charts.DotChart exposing (dotChartView)
import Charts.WaterfallChart exposing (waterfallChartView)
import Charts.DoubleCharts exposing (doubleChartsView)
import SharingStyle exposing (svgChartStyle)
import Util exposing (legendIsMiddle, getChartPosition)
import SharingView exposing ( legendView, dataTabView, settingsTabView)


view : ChartModel -> Html Msg
view model =
    div [ Html.Attributes.class "container"]
        [ div [ Html.Attributes.class "header" , onClick <| DropDownManager -1 ]
              [ h2 [Html.Attributes.class "title"][text "Créer un graphique"]
              ]
        , div [ Html.Attributes.class "body" ]
              [ div [ Html.Attributes.class "charts-tabsbar", style "background" "#F8F8F8", onClick <| DropDownManager -1]
                    [ div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 0 then "#FFFFFF" else "#F8F8F8"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 0 then "block" else "none"
                                , style "top" "-470px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 0
                                , src <| if model.chartsTabs == 0 then
                                          "../assets/icones-svg/charts-navbar/Rose/bar-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/bar-chart.svg"
                                ] []
                          , div [ style "display" "none"
                                ] []
                          ]
                    , div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 1 then "#FFFFFF" else "#F8F8F8"
                          -- , style "flex" "1"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 1 then "block" else "none"
                                , style "top" "-425px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 1
                                , src <| if model.chartsTabs == 1 then
                                          "../assets/icones-svg/charts-navbar/Rose/Horizontal-bar-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/horizontal-bar-chart.svg"
                                ] []
                          , div [ style "display" <| if model.chartsTabs == 0 then "block" else "none"
                                ] []
                          ]
                    , div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 2 then "#FFFFFF" else "#F8F8F8"
                          -- , style "flex" "1"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 2 then "block" else "none"
                                , style "top" "-381px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 2
                                , src <| if model.chartsTabs == 2 then
                                          "../assets/icones-svg/charts-navbar/Rose/Pie-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/pie-chart.svg"
                                ] []
                          , div [ style "display" <| if model.chartsTabs == 1 then "block" else "none"
                                ] []
                          ]
                    , div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 3 then "#FFFFFF" else "#F8F8F8"
                          -- , style "flex" "1"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 3 then "block" else "none"
                                , style "top" "-337px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 3
                                , src <| if model.chartsTabs == 3 then
                                          "../assets/icones-svg/charts-navbar/Rose/Curve-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/curve-chart.svg"
                                ] []
                          , div [ style "display" <| if model.chartsTabs == 2 then "block" else "none"
                                ] []
                          ]
                    , div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 4 then "#FFFFFF" else "#F8F8F8"
                          -- , style "flex" "1"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 4 then "block" else "none"
                                , style "top" "-293px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 4
                                , src <| if model.chartsTabs == 4 then
                                          "../assets/icones-svg/charts-navbar/Rose/Area-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/area-chart.svg"
                                ] []
                          , div [ style "display" <| if model.chartsTabs == 3 then "block" else "none"
                                ] []
                          ]
                    , div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 5 then "#FFFFFF" else "#F8F8F8"
                          -- , style "flex" "1"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 5 then "block" else "none"
                                , style "top" "-249px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 5
                                , src <| if model.chartsTabs == 5 then
                                          "../assets/icones-svg/charts-navbar/Rose/Dot-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/dot-chart.svg"
                                ] []
                          , div [ style "display" <| if model.chartsTabs == 4 then "block" else "none"
                                ] []
                          ]
                    , div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 6 then "#FFFFFF" else "#F8F8F8"
                          -- , style "flex" "1"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 6 then "block" else "none"
                                , style "top" "-205px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 6
                                , src <| if model.chartsTabs == 6 then
                                          "../assets/icones-svg/charts-navbar/Rose/Waterfall-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/waterfall-chart.svg"
                                ] []
                          , div [ style "display" <| if model.chartsTabs == 5 then "block" else "none"
                                ] []
                          ]
                    , div [ Html.Attributes.class "tabs-items"
                          , style "background" <| if model.chartsTabs == 7 then "#FFFFFF" else "#F8F8F8"
                          , style "flex" "1"
                          ]
                          [ div [ style "display" <| if model.chartsTabs == 7 then "block" else "none"
                                , style "top" "-161px"
                                ] []
                          , img [ onClick <| Update.ChangeChartsTabs 7
                                , src <| if model.chartsTabs == 7 then
                                          "../assets/icones-svg/charts-navbar/Rose/bar-&-curve-chart-rose.svg"
                                        else
                                          "../assets/icones-svg/charts-navbar/Gris/bar-&-curve-chart.svg"
                                ] []
                          , div [ style "display" <| if model.chartsTabs == 6 then "block" else "none"
                                ] []
                          ]
                    ]
              , div [ Html.Attributes.class "content"  ]
                    [ chartView model
                    ]
              ]

        ]



chartView :  ChartModel -> Html Msg
chartView  model =
  let
    settings = model.settings

    settingsTab =
      case model.chartsTabs of
        0 -> model.barChart.current_settingsTab
        1 -> model.horizontalBarChart.current_settingsTab
        2 -> model.pieChart.current_settingsTab
        3 -> model.curveChart.current_settingsTab
        4 -> model.areaChart.current_settingsTab
        5 -> model.dotChart.current_settingsTab
        6 -> model.waterfallChart.current_settingsTab
        7 -> model.doubleChart.current_settingsTab
        _ -> model.barChart.current_settingsTab


    chart =
      case model.chartsTabs of
        0 -> if not model.barChart.stacked then barChartView model else  stackedView model (Shape.stack <| config model.displayData)
        1 ->if not model.barChart.stacked then horizontalBarChartView model else  horizontalStackedView model (Shape.stack <| horizontalConfig model.displayData)
        2 -> pieChartView model
        3 -> curveChartView model
        4 -> areaChartView model
        5 -> dotChartView model
        6 -> waterfallChartView model
        7 -> doubleChartsView model
        _ -> barChartView model

    chartName =
      case model.chartsTabs of
        0 -> "barChart"
        1 -> "horizontalBarChart"
        2 -> "pieChart"
        3 -> "curveChart"
        4 -> "areaChart"
        5 -> "dotChart"
        6 -> "waterfallChart"
        7 -> "doubleChart"
        _ -> "barChart"

    middlePos = legendIsMiddle model

    axeXPos = ((round model.width ) // 2 + (if middlePos then -100 else 0), (round model.height //2)+140)

    axeYPos = ( (round model.width ) // 2 + (if middlePos then 25 else 0),(round model.height //2 * -1)-90 )

    chartPos = getChartPosition { width= model.width
                                , height = model.height
                                , titlePosition = settings.titlePosition
                                , legendPosition = settings.legendPosition
                                , sourcePosition= settings.sourcePosition
                                , chartsTabs = model.chartsTabs
                                }

    nbOfColumns =
        model.displayData
            |> List.head
            |> Maybe.withDefault []
            |> List.length
  in
    div []
        [ div [ Html.Attributes.class "chart-container", onClick <| DropDownManager -1 ]
              [ div []
                  [ svg [ viewBox 0 0 model.width (model.height + 100)
                        , TypedSvg.Attributes.class [ "svg-chart-container" ]
                        , onLoad (if settings.animated then StartAnimation model.height model.width else Animated False)
                        ]
                        [ TypedSvg.style [] [ TypedSvg.Core.text (svgChartStyle model) ]
                        , svg [ viewBox ((Tuple.first chartPos)-20) (Tuple.second chartPos ) model.width model.height
                              -- , TypedSvg.Attributes.class [ "svg-chart" ]
                              ]
                              [ chart
                              , text_ [ x <| toFloat <| Tuple.first axeXPos
                                      , y <| toFloat <| Tuple.second axeXPos
                                      , textAnchor AnchorMiddle
                                      , TypedSvg.Attributes.class [ "axe-x" ]
                                      ]
                                      [ TypedSvg.Core.text settings.axeX ]
                              , text_ [ x <| toFloat <| Tuple.first axeYPos
                                      , y <| toFloat <| Tuple.second axeYPos
                                      , textAnchor AnchorMiddle
                                      , TypedSvg.Attributes.class [ "axe-y" ]
                                      ]
                                      [TypedSvg.Core.text settings.axeY ]
                              ]
                        , if  model.chartsTabs == 6 || model.chartsTabs == 5 then
                            g [] []
                          else
                            legendView  model
                        , g []
                            [ text_ [ TypedSvg.Attributes.class [ "svg-chart-title" ]
                                    , TypedSvg.Attributes.visibility <| if settings.showTitle then "visible" else "hidden"
                                    , x <| Tuple.first settings.titlesPos
                                    , y <| Tuple.second settings.titlesPos
                                    , textAnchor (if String.contains "center" settings.titlePosition then AnchorMiddle else AnchorInherit )
                                    ]
                                    [ TypedSvg.Core.text <|settings.title ]
                            , text_ [ TypedSvg.Attributes.class [ "svg-chart-source" ]
                                    , TypedSvg.Attributes.visibility <| if settings.showSource then "visible" else "hidden"
                                    , x <| Tuple.first settings.sourcePos
                                    , y <| Tuple.second settings.sourcePos
                                    , textAnchor (if String.contains "center" settings.sourcePosition then AnchorMiddle else AnchorInherit )
                                    ]
                                    [ TypedSvg.Core.text <|settings.source ]
                            ]
                        ]
                  -- , svg [ viewBox 0 0 model.width model.height ]
                  --       [ rect
                  --           [ TypedSvg.Attributes.InPx.x <| model.padding.top*2
                  --           , TypedSvg.Attributes.InPx.y <|  model.padding.top/2
                  --
                  --           , TypedSvg.Attributes.InPx.width <| model.width - (model.padding.top * 4)
                  --           , TypedSvg.Attributes.InPx.height <| model.padding.top/3
                  --           , TypedSvg.Attributes.style "fill : grey"
                  --           ]
                  --           [
                  --             -- line [ TypedSvg.Attributes.InPx.x1 <|  0
                  --             --      , TypedSvg.Attributes.InPx.y1 <| 0
                  --             --      , TypedSvg.Attributes.InPx.x2 <| 50
                  --             --      , TypedSvg.Attributes.InPx.y2 <|  20
                  --             --      , strokeWidth 3
                  --             --      , stroke <| Paint Color.black
                  --             --
                  --             --      , TypedSvg.Attributes.style "fill : blak"
                  --             --      ][]
                  --
                  --           ]
                        -- ]
                  , if (model.chartsTabs == 2 ) then
                      div [ Html.Attributes.style "textAlign" "center"
                          , Html.Attributes.style "margin" "20px"
                          ]
                          [ input
                            [ type_ "range"
                            , onInput SetPieChartColumn
                            , value <| String.fromInt model.pieChart.colName
                            , Html.Attributes.min "0"
                            , Html.Attributes.max <| String.fromInt (nbOfColumns - 2)
                            , step "1"
                            , Html.Attributes.class "slider"
                            ]
                            []
                          ]
                    else if (model.chartsTabs == 6 ) then
                      div [ Html.Attributes.style "textAlign" "center"
                          , Html.Attributes.style "margin" "20px"
                          ]
                          [ input
                            [ type_ "range"
                            , onInput SetWaterfallColumn
                            , value <| String.fromInt model.waterfallChart.colIndex
                            , Html.Attributes.min "0"
                            , Html.Attributes.max <| String.fromInt (nbOfColumns - 2)
                            , step "1"
                            , Html.Attributes.class "slider"
                            ]
                            []
                          ]
                    else
                      div [] []
                  ]
              ]
        , div [ Html.Attributes.class "settings-container" ]
              [
              div []
                    [ div [ Html.Attributes.class "tabs" , onClick <| DropDownManager -1]
                          [ button [ Html.Attributes.style "color" (if settingsTab == 0 then "#FF335A" else "#A5A5A5")
                                   , Html.Attributes.style "backgroundColor" (if settingsTab == 0 then "#FFE6EA" else "#FFFFFF")
                                   , Html.Attributes.style "borderBottom" (if settingsTab == 0 then "1.5px solid #FF335A" else "1.5px solid #FFFFFF")
                                   , onClick <| ChangeSettingsTab chartName 0
                                   ]
                                   [ Html.text "Données"]
                          , button [ Html.Attributes.style "color" (if settingsTab == 1 then "#FF335A" else "#A5A5A5")
                                   , Html.Attributes.style "backgroundColor" (if settingsTab == 1 then "#FFE6EA" else "#FFFFFF")
                                   , Html.Attributes.style "borderBottom" (if settingsTab == 1 then "1.5px solid #FF335A" else "1.5px solid #FFFFFF")
                                   , onClick <| ChangeSettingsTab chartName 1
                                   ]
                                   [ Html.text "Paramètres"]
                          ]
                    , div [ Html.Attributes.class "tabs-content" ]
                          [
                            (if settingsTab == 0 then
                               dataTabView model
                             else
                              settingsTabView model  {name = chartName
                                                    , stack = (if chartName == "barChart" || chartName == "horizontalBarChart" || chartName == "doubleChart" then model.barChart.stacked else False)
                                                    , isLine = (if chartName == "curveChart" || chartName == "doubleChart" || chartName == "areaChart"then model.curveChart.inLine else False)} )
                    ]
                    , div [Html.Attributes.class "validation", onClick <| DropDownManager -1]
                          [ button [] [Html.text "Valider"]
                          , button [] [Html.text "Annuler"]
                          ]
                    ]


              ]
        ]
