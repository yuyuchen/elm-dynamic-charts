module Charts.BarChart exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Attributes exposing (..)
import Axis
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, text_, svg)
import TypedSvg.Attributes exposing (class, textAnchor, dominantBaseline, transform, viewBox, fill)
import TypedSvg.Attributes.InPx exposing (x, y)
import TypedSvg.Events exposing (onLoad,onDblClick)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (AnchorAlignment(..), DominantBaseline(..), Transform(..), Paint(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)
import Json.Decode as Decode exposing (at)
import List.Extra exposing (uncons)
import Array
import Array2D

-- Prezance packages --
import Update exposing (Msg(..))
import Model exposing (ChartModel, PrzTableCell)
import SharingStyle exposing (barsStyle, svgChartStyle, stackStyle)
import Util exposing ( getAbscissesAndColors, column, legendIsMiddle, formatData)




barChartView : ChartModel -> Svg Msg
barChartView model =
    let

        ( listColors, abscisses ) =
            getAbscissesAndColors model.displayData

        sub =  model.height - ( Scale.convert (yScale model) 0) - (2 * model.padding.top)

    in
    g []
        [ barsStyle listColors model
        , g [ transform [ Translate model.padding.top model.padding.top ], TypedSvg.Attributes.class [ "series" ] ]
            (List.map (column model (xScale model abscisses)) abscisses)
        , g [ transform [ Translate (model.padding.top - 1) (model.height - model.padding.top - sub) ], TypedSvg.Attributes.class [ "svg-axes-x" ]]
            [ xAxis model abscisses ]
        , g [ transform [ Translate (model.padding.top - 1) model.padding.top ], TypedSvg.Attributes.class [ "svg-axes-y" ] ]
            [ yAxis model ]
        ]


stackedView : ChartModel -> StackResult String  -> Svg Msg
stackedView model { values, labels, extent } =
    let
        -- transpose back to get the values per year

        negativeConfig =
          nConfig model.displayData
          |> Shape.stack

        nLabels =  negativeConfig.labels
        nYearValues = List.Extra.transpose <| negativeConfig.values


        headerWithColor : List String -> List (List PrzTableCell)
        headerWithColor labelList = List.map (\l->
                              List.map (\hd -> if l == hd.name then hd else PrzTableCell "" "")  getHeaderRow
                              ) labelList

        getHeaderRow =  List.drop 1 <| List.map (\d ->  Maybe.withDefault (PrzTableCell "" "") (List.head d)) model.displayData


        yearValues =
            List.Extra.transpose values

        abscisses =
            listOfAbscisses model.displayData

        xScaleStacked : BandScale String
        xScaleStacked =
          abscisses
            |> Scale.band
                { defaultBandConfig | paddingInner = 0.1, paddingOuter = 0.2 } ( 0, model.width - (model.padding.top + model.padding.bottom) - (if legendIsMiddle model  then 140 else 0))

        xAxisStacked : Svg msg
        xAxisStacked =
          Axis.bottom [ Axis.tickCount 10 ] (Scale.toRenderable (\s -> s) (xScaleStacked))

        yScaleStacked : ContinuousScale Float
        yScaleStacked =
            Scale.linear ( model.height - (model.padding.left + model.padding.right) , 0 ) (Tuple.first negativeConfig.extent , Tuple.second extent)
                |> Scale.nice 5

        scaledValues =
            List.map
                (
                  List.map (\( y1, y2 ) ->
                                ( Scale.convert yScaleStacked y1, Scale.convert yScaleStacked y2 )
                            )
                ) yearValues

        nScaledValues =
            List.map
                (
                  List.map (\( y1, y2 ) ->
                                ( Scale.convert yScaleStacked y1, Scale.convert yScaleStacked y2 )
                            )
                ) nYearValues


        ( listColors, absc ) =
            getAbscissesAndColors model.displayData

        t =
            Transition.value model.barChart.transition

        sub =  model.height - ( Scale.convert yScaleStacked 0) - (2 * model.padding.top)

        nbRow =
          scaledValues
          |> List.head
          |> Maybe.withDefault []
          |> List.length

    in
    g []
        [ barsStyle listColors model
        , stackStyle abscisses nbRow model
        , g [ transform [ Translate model.padding.left model.padding.top ], TypedSvg.Attributes.class [ "series" ] ] <|
            List.map (stackedColumn True model (headerWithColor labels) (xScaleStacked)) (List.map2 (\a b -> ( a, b )) abscisses scaledValues)
        , g [ transform [ Translate model.padding.left model.padding.top ], TypedSvg.Attributes.class [ "series" ] ] <|
            List.map (stackedColumn False model (headerWithColor nLabels) (xScaleStacked)) (List.map2 (\a b -> ( a, b )) abscisses nScaledValues)
        , g [ TypedSvg.Attributes.class [ "block" ]]
            [ rect
                [ x <| 50
                , y <| 50 - 1
                , TypedSvg.Attributes.InPx.width <| model.width - 105
                , TypedSvg.Attributes.InPx.height <| if t >= 0 then t else 0
                , fill <| Paint Color.black
                ]
                []

            ]
        , g [ transform [ Translate (model.padding.left - 1) (model.height - model.padding.bottom - sub) ] , TypedSvg.Attributes.class [ "svg-axes-x" ]]
            [ xAxisStacked ]
        , g [ transform [ Translate (model.padding.left - 1) model.padding.top ] , TypedSvg.Attributes.class [ "svg-axes-y" ]]
            [ Axis.left [] (yScaleStacked) ]

        ]




---------- Stacked Barchart View -------------------


stackedColumn : Bool -> ChartModel -> List (List PrzTableCell) -> BandScale String -> (String, List ( Float, Float ))  ->  Svg Msg
stackedColumn isPositiveVal model headerOrdered bScale ( year, values )  =
    let
        getColors =
          List.map (\x -> "#"++x)
          <| String.split "#"
          <| String.dropLeft 1
          <| String.join ""
          <| List.concat
          <| List.map (\i -> List.map (\j -> j.color) i) headerOrdered

        getColIndex =
          model.displayData
          |> List.head
          |> Maybe.withDefault []
          |> List.map .name
          |> List.indexedMap Tuple.pair
          |> List.map (\(i, cell)-> if cell == year then (String.fromInt i) else "")
          |> String.concat
          |> String.toInt
          |> Maybe.withDefault 0

        block : (Int, String) -> (Float, Float) -> Svg Msg
        block (row, c) ( upperY, lowerY ) =
          let
            color =  hexToColor c |> Result.withDefault Color.gray

            getCell =
              model.displayData
              |> Array2D.fromList
              |> .data
              |> Array.get (row + 1)
              |> Maybe.withDefault (Array.fromList [])
              |> Array.get getColIndex
              |> Maybe.withDefault (PrzTableCell "" "")
              |> .name

            (val ,simple) = formatData getCell model.settings

            (defaultVal, simpleVal) =
              if String.contains "-" getCell then
                ("-"++val, "-"++simple)
              else
                (val ,simple)
          in
          g [TypedSvg.Attributes.class [ "stack" ++ year ++  (String.fromInt row)], onDblClick <| Stacked (not model.barChart.stacked)]
            [ rect
                  [ x <| Scale.convert bScale (year)
                  , y <| if isPositiveVal then lowerY else upperY
                  , TypedSvg.Attributes.InPx.width <| Scale.bandwidth bScale
                  , TypedSvg.Attributes.InPx.height <| (abs <| upperY - lowerY)
                  , fill <| Paint color
                  ]
                  []
            , text_
              [ x <| Scale.convert bScale (year) + (Scale.bandwidth bScale /2 ) - 25
              , y <| 0
              -- , textAnchor AnchorMiddle
              , TypedSvg.Attributes.class [ "data-value" ]
              ]
              [text <| if model.settings.simpleData && simpleVal /= "" then simpleVal else defaultVal ]
            ]

    in
    g [ TypedSvg.Attributes.class [ "column" ] ]
      (List.map2 block (List.indexedMap Tuple.pair getColors) values)
        -- (List.map2 block (listOfColors) values)



listOfAbscisses : List (List PrzTableCell) -> List String
listOfAbscisses data =
  List.head data
  |> Maybe.withDefault []
  |> List.drop 1
  |> List.map .name
  -- List.map .year crimeRates

config : List (List PrzTableCell) -> StackConfig String
config d =
   { data = (dataRows d)
   , offset = Shape.stackOffsetNone
   , order =
        -- stylistic choice: largest (by sum of values) category at the bottom
        -- List.sortBy (Tuple.second >> List.sum >> negate)
       List.map (\r-> r)
   }

nConfig : List (List PrzTableCell) -> StackConfig String
nConfig d =
   { data = (nDataRows d)
   , offset = Shape.stackOffsetNone
   , order =
        -- stylistic choice: largest (by sum of values) category at the bottom
        -- List.sortBy (Tuple.second >> List.sum >> negate)
       List.map (\r-> r)
   }



dataRows : List (List PrzTableCell) -> List (String, List Float)
dataRows data =
  List.tail data
  |> Maybe.withDefault []
  |> List.map List.Extra.uncons
  |> List.map (Maybe.withDefault (PrzTableCell "" "", []))
  |> List.map (\(a, slist) ->
                    ( a.name , List.map (\tableH -> if (Maybe.withDefault 0 (String.toFloat tableH.name)) <= 0 then 0 else (Maybe.withDefault 0 (String.toFloat tableH.name))) slist)
              )

nDataRows : List (List PrzTableCell) -> List (String, List Float)
nDataRows data =
  List.tail data
  |> Maybe.withDefault []
  |> List.map List.Extra.uncons
  |> List.map (Maybe.withDefault (PrzTableCell "" "", []))
  |> List.map (\(a, slist) ->
                    ( a.name , List.map (\tableH -> if (Maybe.withDefault 0 (String.toFloat tableH.name)) > 0 then 0 else (Maybe.withDefault 0 (String.toFloat tableH.name))) slist)
              )



---------------------------- Bar Chart function --------------------------------

xAxis : ChartModel -> List ( String, List String ) -> Svg msg
xAxis model data =
    Axis.bottom [] (Scale.toRenderable identity (xScale model data))


xScale : ChartModel -> List ( String, List String ) -> BandScale String
xScale model data =
    let
        ( str, innerList ) =
            List.head data
                |> Maybe.withDefault ( "", [] )

        x =
            toFloat <| List.length innerList

        mydata = data ++ [("", [])]

        padI =
            1 - (1 / (x + 1))
    in
    List.map Tuple.first mydata
        |> Scale.band { defaultBandConfig | paddingInner = padI, paddingOuter = 0.1 }
            ( 0 , model.width - (if legendIsMiddle model  then 6 else 2 ) * model.padding.top )


yScale : ChartModel -> ContinuousScale Float
yScale model =
    -- Scale.linear ( model.height - 2 * model.padding.top, 0 ) ( 0, model.maxHeightScale )
    Scale.linear
        ( model.height - 2 * model.padding.top
        , if legendIsMiddle model  then
            -50

          else
            0
        )
        ( model.minHeightScale, model.maxHeightScale )


yAxis : ChartModel -> Svg msg
yAxis model =
    Axis.left [ Axis.tickCount 5 ] (yScale model)
