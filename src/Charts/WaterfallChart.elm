module Charts.WaterfallChart exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Attributes exposing (..)
import Axis
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, text_, svg, line)
import TypedSvg.Attributes exposing (class, textAnchor, dominantBaseline, transform, viewBox, fill)
import TypedSvg.Attributes.InPx exposing (x, y)
import TypedSvg.Events exposing (onLoad)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (AnchorAlignment(..), DominantBaseline(..), Transform(..), Paint(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)
import Json.Decode as Decode exposing (at)
import List.Extra exposing (uncons)
import Array


-- Prezance packages --
import Update exposing (Msg)
import Model exposing (ChartModel, PrzTableCell, initialTableHeader, ChartSettingsModel)
import Util exposing (legendIsMiddle, formatData, convertToWaterfalldata, dataOnly, cumulData)
import SharingStyle exposing (waterfallStyle)



waterfallChartView :  ChartModel -> Svg msg
waterfallChartView  model =
    let
        ( listColors, abscisses ) =
            getAbscissesAndColors model.data

        myData = convertToWaterfalldata model
        sub =  model.height - ( Scale.convert (yScale model) 0) - (2 * model.padding.top)

        colName =
          .name
          <| Maybe.withDefault  (PrzTableCell "" "")
          <| Array.get (model.waterfallChart.colIndex + 1)
          <| Array.fromList
          <| Maybe.withDefault []
          <| List.head
          <| model.data
    in
    g []
        [ waterfallStyle listColors model
        , g [ transform [ Translate model.padding.top model.padding.top ], TypedSvg.Attributes.class [ "series" ] ]
            -- (List.map (column model (xScale model abscisses)) abscisses)
            (List.map (column model (xScale model myData)) (List.indexedMap Tuple.pair myData))

        , g []
            [ rect
               [ x <| model.padding.top
               , y <| model.padding.top - 2
               , TypedSvg.Attributes.InPx.width <|  model.width -  model.padding.top* 2
               , TypedSvg.Attributes.InPx.height <| Transition.value model.waterfallChart.transition
               , fill <| Paint <| (hexToColor model.settings.backgroundColor |> Result.withDefault Color.gray)
               ]
               []
            ]
        , g [ transform [ Translate (model.padding.top) (model.height ) ]] [ text_ [ Html.Attributes.style "fontSize" "1.5em"] [Html.text colName]]
        , g [ transform [ Translate (model.padding.top - 1) (model.height - model.padding.top - sub ) ] , TypedSvg.Attributes.class [ "svg-axes-x" ]]
            [ xAxis model myData ]
        , g [ transform [ Translate (model.padding.top - 1) (model.padding.top ) ] , TypedSvg.Attributes.class [ "svg-axes-y" ]]
            [ yAxis model ]
        ]




---------------------------- Bar Chart function --------------------------------

-- for Bar Chart
convertDataToFloat : String -> ChartSettingsModel -> Float
convertDataToFloat number settings =
  let
    decimalSeparator = settings.decimalSeparator
    thousandsSeparator = settings.thousandsSeparator
    sufix = settings.dataSuffix
    prefix = settings.dataPrefix
    delSeparators =  String.replace prefix "" <| String.replace sufix "" <| String.replace thousandsSeparator ""  number
    val = if decimalSeparator == "," then String.replace decimalSeparator "." delSeparators else delSeparators
  in
    Maybe.withDefault 0 <| String.toFloat val


getAbscissesAndColors : List (List PrzTableCell) -> ( List String, List ( String, List String ) )
getAbscissesAndColors data =
    ( List.map List.Extra.uncons data
        |> List.map (Maybe.withDefault ( initialTableHeader, [] ))
        |> List.map Tuple.first
        |> List.map .color
    , List.Extra.transpose data
        |> List.map List.Extra.uncons
        |> List.map (Maybe.withDefault ( initialTableHeader, [] ))
        |> List.map (\( a, b ) -> ( a.name, List.map .name b ))
        |> List.drop 1
    )



column : ChartModel -> BandScale String -> ( Int ,(String, String) ) -> Svg msg
column tmodel bscale (i, (date, str)) =
    let
        (sci, simpleData) =  formatData str tmodel.settings
        val =  convertDataToFloat sci tmodel.settings
        stringVal = if tmodel.settings.simpleData && simpleData /= "" then simpleData else sci

        arrayData = dataOnly tmodel |> Array.fromList
        cumulDataList = cumulData tmodel 1 (Array.toList arrayData) |> Array.fromList

        -- _ = Debug.log "water = " simpleData


        cumulVal = Array.get i cumulDataList |> Maybe.withDefault 0
        sub =  tmodel.height - ( Scale.convert (yScale tmodel) 0) - (2 * tmodel.padding.top)
        currentVal = Array.get i cumulDataList |> Maybe.withDefault 0
        previousVal =  Array.get (i-1) cumulDataList |> Maybe.withDefault 0
        nextVal =  Array.get (i+1) cumulDataList |> Maybe.withDefault 0

        scaleConvY =
          if ( not (List.member i tmodel.waterfallChart.soldRows))then
            -- Scale.convert (yScale tmodel) val
            if cumulVal < previousVal then
              Scale.convert (yScale tmodel) previousVal
            else
              Scale.convert (yScale tmodel) cumulVal
          else
            Scale.convert (yScale tmodel) (if cumulVal > 0 then (cumulVal) else 0)

        scaleConvX =
          Scale.convert bscale date  - 2

        h =
          if (not (List.member i tmodel.waterfallChart.soldRows) )then
            if cumulVal < previousVal then
              Scale.convert (yScale tmodel) previousVal + (tmodel.height - (Scale.convert (yScale tmodel) cumulVal) - (2 * tmodel.padding.top))
            else
              Scale.convert (yScale tmodel) cumulVal + (tmodel.height - (Scale.convert (yScale tmodel) previousVal) - (2 * tmodel.padding.top))
          else
            if cumulVal > 0 then
              Scale.convert (yScale tmodel) cumulVal + sub
            else
              Scale.convert (yScale tmodel) 0  + (tmodel.height - (Scale.convert (yScale tmodel) cumulVal) - (2 * tmodel.padding.top))

        rectHeight =  tmodel.height - h - (2 * tmodel.padding.top)

        soldColor = hexToColor tmodel.waterfallChart.totalBarColor |> Result.withDefault Color.gray
        positiveColor = hexToColor tmodel.waterfallChart.positiveBarColor |> Result.withDefault Color.gray
        negativeColor = hexToColor tmodel.waterfallChart.negativeBarColor |> Result.withDefault Color.gray
    in
    g [] [
          g [ TypedSvg.Attributes.class [ "bar" ++ (String.fromInt i)] ]
            [ rect
                [ x <| scaleConvX
                , y <| scaleConvY
                , TypedSvg.Attributes.InPx.width <| Scale.bandwidth bscale
                , TypedSvg.Attributes.InPx.height rectHeight
                , fill <| Paint <|  if (List.member i tmodel.waterfallChart.soldRows) then soldColor else if cumulVal < previousVal then negativeColor else positiveColor
                  -- if  cumulVal < previousVal then  Color.red else if (List.member i tmodel.waterfallChart.soldRows) then color else Color.green
                ]
                []
           , text_ [x <| scaleConvX + ( Scale.bandwidth bscale / 2 )
                   , y <| scaleConvY + ( rectHeight / 2)
                   , textAnchor AnchorMiddle
                   , TypedSvg.Attributes.class [ "data-value" ]
                   ]
                   [text <| if (List.member i tmodel.waterfallChart.soldRows) then stringVal else if cumulVal < previousVal then ("-" ++ stringVal) else ("+" ++ stringVal) ]
                   --if model.settings.simpleData && simpleData /= "" then simpleData else formattedData
                   --simpleData

            ]

         , rect -- line
              [ x <| scaleConvX
              , y <| Scale.convert (yScale tmodel) cumulVal
              , TypedSvg.Attributes.InPx.width <| (Scale.bandwidth bscale) + (if i < ((Array.length arrayData)-1) then (Scale.bandwidth bscale / 2 + (Scale.bandwidth bscale) - 10) else 0)
              , TypedSvg.Attributes.InPx.height 1
              , fill <| Paint <| (hexToColor tmodel.waterfallChart.lineColor |> Result.withDefault Color.gray)
              ]
              []
         ]
        -- (List.map funcColumn (List.indexedMap Tuple.pair listNW))





xAxis : ChartModel -> List ( String, String ) -> Svg msg
xAxis model data =
    Axis.bottom [] (Scale.toRenderable identity (xScale model data))


xScale : ChartModel -> List ( String, String ) -> BandScale String
xScale model data =
    let
        x =
            toFloat <| List.length data

        padI =
            1 - (1 / (x + 1))
    in
    List.map Tuple.first data
        |> Scale.band { defaultBandConfig | paddingInner = 0.3, paddingOuter = 0.1 }
            ( 0 , model.width - (if legendIsMiddle model  then 6 else 2 ) * model.padding.top  )


yScale : ChartModel -> ContinuousScale Float
yScale model =
    -- Scale.linear ( model.height - 2 * model.padding.top, 0 ) ( 0, model.maxHeightScale )
    Scale.linear
        ( model.height - 2 * model.padding.top
        , if legendIsMiddle model  then
            -50

          else
            0
        )
        ( model.waterfallChart.minHeight, model.waterfallChart.maxHeight)


yAxis : ChartModel -> Svg msg
yAxis model =
    Axis.left [ Axis.tickCount 5 ] (yScale model)
