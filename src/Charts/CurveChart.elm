module Charts.CurveChart exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Events exposing (onClick)
import Array
import Shape
import Axis
import Path exposing (Path)
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import TypedSvg exposing (svg, g, text_, rect)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Attributes exposing (stroke, fill, transform, viewBox, textAnchor )
import TypedSvg.Attributes.InPx exposing (strokeWidth, height, width, x, y)
import TypedSvg.Events exposing (onLoad)
import TypedSvg.Types exposing (Paint(..), Transform(..), AnchorAlignment(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)

-- Prezance packages --
import Update exposing (Msg(..))
import Model exposing (ChartModel)
import SharingStyle exposing (lineChartStyle, svgChartStyle)
import SharingView exposing (drawLine, dataOnHover)
import Util exposing
  ( getChartPosition
  , legendIsMiddle
  , formatData
  , lineFormatData
  , getLineDataBody
  , getLinesColor
  , getAbscAndVAlue
  )


------------------- Sub View --------------------------------------

curveChartView : ChartModel  -> Svg Msg
curveChartView model =
  let
    data = lineFormatData model
    dataHeader = Maybe.withDefault [] <| List.head <| data
    colorsList = getLinesColor model
    dataWithColor = List.map2 Tuple.pair colorsList data
    abscAndVAlue = getAbscAndVAlue model
    dataBody = getLineDataBody model

    transX =  Transition.value model.curveChart.transitionX
    transWidth =  Transition.value model.curveChart.transitionWidth

    sub =  model.height - ( Scale.convert (yScale model) 0) - (2 * model.padding.top)
  in
    g []
        [ lineChartStyle  model dataBody

        , g [ transform [ Translate model.padding.top model.padding.top ] ]
            (List.map (drawLine model) dataWithColor)
        , g [] <| List.map (\r -> g [] <| List.map (\(x, y) -> dataOnHover model (x ,(Tuple.first y + model.padding.top, Tuple.second y + model.padding.top)) ) r )  abscAndVAlue
        , g []
            [ rect
                [ TypedSvg.Attributes.InPx.x <| transX  --model.padding.top + 2
                , TypedSvg.Attributes.InPx.y <|  model.padding.top - 5 + (if legendIsMiddle model  then -50 else 0)
                , TypedSvg.Attributes.InPx.width <| transWidth
                , TypedSvg.Attributes.InPx.height <| model.height - (model.padding.top * 2) + 10 + (if legendIsMiddle model  then 50 else 0)
                , TypedSvg.Attributes.style ("fill : " ++ model.settings.backgroundColor)
                ] []
            ]
        , g [ transform [ Translate (model.padding.top - 1) (model.height - model.padding.top - sub) ] , TypedSvg.Attributes.class [ "svg-axes-x" ]]
            [ xAxis dataHeader model]
        , g [ transform [ Translate (model.padding.top - 1) (model.padding.top) ], TypedSvg.Attributes.class [ "svg-axes-y" ] ]
            [ yAxis model]
        ]




---------------------------- Curve Chart function --------------------------------

xScale :  List ( String, Float )-> ChartModel ->  BandScale String
xScale  data model =
    List.map Tuple.first data |>
      -- Scale.band { defaultBandConfig | paddingInner = 20, paddingOuter = 0 } ( 0, model.width - 2 * 50 )
      Scale.band { defaultBandConfig | paddingInner = 20, paddingOuter = 0 } ( 0, if legendIsMiddle model  then model.width - 250  else model.width - 2 * 50)


yScale : ChartModel ->  ContinuousScale Float
yScale model =
    -- Scale.linear ( model.height - 2 * model.padding.top, 0 ) ( 0, model.maxHeightScale )
    Scale.linear ( model.height - 2 * model.padding.top
                  , if legendIsMiddle model  then -50 else 0
                  ) ( model.minHeightScale , model.maxHeightScale )


xAxis : List ( String, Float ) -> ChartModel -> Svg msg
xAxis listData model =
  let
    data = Maybe.withDefault [] <| List.head <| lineFormatData model
  in
    Axis.bottom [ Axis.tickCount (List.length data) ] (Scale.toRenderable identity (xScale data model ))


yAxis : ChartModel -> Svg msg
yAxis model =
    Axis.left [ Axis.tickCount 5 ] <| yScale model
