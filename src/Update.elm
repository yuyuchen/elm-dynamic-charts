module Update exposing (..)

-- Elm packages --
import Html exposing (..)
import Task exposing (..)
import Regex
import Array2D
import Array
import Transition exposing (Transition)
import Interpolation exposing (Interpolator)
import List.Extra exposing ( transpose)


-- Prezance packages --
import Model exposing (..)


type Msg =
    ChangeChartsTabs Int
  | ChangeSettingsTab String Int
  | DropDownManager Int
  | KeyDown Int Int Int
  -- | InitMaxAxe
  -- Animation
  | Tick Int
  | StartAnimation Float Float
  | Animated Bool
  | InitData

  | HiddenRow Int


  -- bar chart
  | Stacked Bool
  -- curve chart
  | InLine Bool
  -- double chart
  | ChangeChart Int String
  | UpdateDoubleChartData
  -- pie chart
  | SetPieChartColumn String
  -- area chart
  | SetOrderAreaChart String String
  -- waterfall chart
  | SetFlux Int String
  | SetChartColor String String
  | SetWaterfallColumn String
  -- dot chart
  | SetDotColor String String

  -- data update
  | ChooseGraphLineColor Int String
  | TransposeTableData
  | AddNewCol
  | AddNewLine
  | TableCellTempValue Int Int String
  | UpdateChartTableData ( Int, Int )


  -- Chart settings update

  -- main style settings
  | SetGlobalFontFamily String
  | SetChartBackground String
  | SetTxtAttributes String String String

  -- header settings
  | SetTitleVisibility Bool
  | SetSourceVisibility Bool
  | SetChartTitle String
  | SetChartSource String
  | SetTitlePos String
  | SetSourcePos String

  -- axes settings
  | SetAxeX String
  | SetAxeY String
  | SetAxesVisibility Bool

  -- data value settings
  | SetValueVisibility Bool
  | SetSimpleData Bool
  | SetDecimalSeparator String
  | SetThousandsSeparator String
  | SetDataPrefix String
  | SetDataSuffix String

  -- legend settings
  | SetShowLegend Bool
  | SetAutoColorForLegend Bool
  | SetLegendPos String



message : msg -> Cmd msg
message msg =
    Task.perform identity (Task.succeed msg)


getIndexOf : Int -> List String -> Int
getIndexOf val list=
  Maybe.withDefault 0
  <| String.toInt
  <| String.join ""
  <| List.map String.fromInt
  <|String.indexes (String.fromInt val)
  <| String.join ""
  <| list

update : Msg -> ChartModel -> ( ChartModel, Cmd Msg )
update msg model =
  let
      oldChartSettings = model.settings

      oldBarChart = model.barChart
      oldHorizontalBarChart = model.horizontalBarChart
      oldPieChart = model.pieChart
      oldCurveChart = model.curveChart
      oldAreaChart = model.areaChart
      oldDotChart = model.dotChart
      oldWaterfallChart = model.waterfallChart
      oldDoubleChart = model.doubleChart

      legendIsMiddle = String.contains "middle" model.settings.legendPosition

  in
      case msg of

            KeyDown i j key ->
              let
                cmd =
                  if key == 13 then
                    message <| UpdateChartTableData (i, j)
                  else
                    Cmd.none
              in
              ( { model | barChart = oldBarChart },cmd)

            ChangeChartsTabs newtab ->
              ( { model | chartsTabs = newtab}, message <| Animated oldChartSettings.animated)

            ChangeSettingsTab myChart newTab ->
                case myChart of
                  "barChart" -> ( { model | barChart = {oldBarChart | current_settingsTab = newTab }}, Cmd.none )
                  "horizontalBarChart" -> ( { model | horizontalBarChart = {oldHorizontalBarChart | current_settingsTab = newTab }}, Cmd.none)
                  "pieChart" -> ( { model | pieChart = {oldPieChart | current_settingsTab = newTab }}, Cmd.none)
                  "curveChart" -> ( { model | curveChart = {oldCurveChart | current_settingsTab = newTab }}, Cmd.none)
                  "areaChart" -> ( { model | areaChart = {oldAreaChart | current_settingsTab = newTab }}, Cmd.none)
                  "dotChart" -> ( { model | dotChart = {oldDotChart | current_settingsTab = newTab }}, Cmd.none)
                  "waterfallChart" -> ( { model | waterfallChart = {oldWaterfallChart | current_settingsTab = newTab }}, Cmd.none)
                  "doubleChart" -> ( { model | doubleChart = { oldDoubleChart | current_settingsTab = newTab }}, Cmd.none )

                  _ -> ( { model | barChart = oldBarChart}, Cmd.none )

            DropDownManager row ->
              let
                newDropdownState =
                  if row == Tuple.first model.tableDropdown then
                    (row, not <| Tuple.second model.tableDropdown)
                  else if row == -1 then
                    (row, False)
                  else
                    (row, True)

                -- find the maximum value in the data list
                maxNumbFunc : List String -> Float
                maxNumbFunc sub_l =
                    sub_l
                        |> List.map (\v -> Maybe.withDefault 0 (String.toFloat v))
                        |> List.maximum
                        |> Maybe.withDefault 0

                sortRow =
                  Maybe.withDefault []
                  <| List.tail
                  <| List.map (\(x,y) ->x)
                  <| List.sortBy (Tuple.second)
                  <| List.indexedMap Tuple.pair
                  <| List.map maxNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) model.data)
              in
                ( { model | tableDropdown = newDropdownState, areaChart = { oldAreaChart | sortedData = sortRow}}, message <| HiddenRow -1)



            ------------------------------ Animation -----------------------------------

            Tick tick ->
                ( { model | barChart = { oldBarChart | transition = Transition.step tick oldBarChart.transition}
                          , horizontalBarChart = { oldHorizontalBarChart | transition = Transition.step tick oldHorizontalBarChart.transition}
                          , pieChart = { oldPieChart | transition1 = Transition.step tick oldPieChart.transition1, transition2 = Transition.step tick oldPieChart.transition2}
                          , curveChart = { oldCurveChart | transitionX = Transition.step tick oldCurveChart.transitionX, transitionWidth = Transition.step tick oldCurveChart.transitionWidth }
                          , areaChart = { oldAreaChart | transition = Transition.step tick oldAreaChart.transition}
                          , dotChart = { oldDotChart | transition = Transition.step tick oldDotChart.transition}
                          , waterfallChart = { oldWaterfallChart | transition = Transition.step tick oldWaterfallChart.transition}
                          , doubleChart = { oldDoubleChart | transitionX = Transition.step tick oldDoubleChart.transitionX, transitionWidth = Transition.step tick oldDoubleChart.transitionWidth }
                  }
                  , Cmd.none
                )

            StartAnimation barHeight curveWidth->
              let

                newBarChart =
                  if oldBarChart.stacked then
                    { oldBarChart | transition = Transition.for 1500 (Interpolation.float (barHeight - 105) 0) }
                  else
                    { oldBarChart | transition = Transition.for 1500 (Interpolation.float (barHeight) 0) }


                newPieChart =
                  { oldPieChart | transition1 = Transition.for 1500 (Interpolation.float -7 0)
                  , transition2 = Transition.for 1500 (Interpolation.float 0.99 0) }

                newCurveChart =
                  let
                    startWidth = model.width - (model.padding.top * 2) + 5 + (if legendIsMiddle  then -155 else 0)
                  in
                  { oldCurveChart | transitionX = Transition.for 1500 (Interpolation.float 50 (curveWidth - 50))
                                  , transitionWidth = Transition.for 1500 (Interpolation.float startWidth 0) }

                newDoubleChart =
                  let
                    startWidth = model.width - (model.padding.top * 3) - 5 + (if legendIsMiddle  then (model.padding.top * -4) else 0)
                    endPosX = model.width - (model.padding.top * 2)  + (if legendIsMiddle  then (model.padding.top * -4) else 0) - 1
                  in
                  { oldDoubleChart | transitionX = Transition.for 1500 (Interpolation.float (model.padding.top + 2) endPosX )
                                  , transitionWidth = Transition.for 1500 (Interpolation.float startWidth 0)
                  }

                newWaterfallChart =
                  { oldWaterfallChart | transition = Transition.for 1500 (Interpolation.float (model.height -  model.padding.top * 2 + 5) 0 )
                  }

              in
                  ( { model | curveChart = newCurveChart
                            , barChart = newBarChart
                            , doubleChart = newDoubleChart
                            , waterfallChart = newWaterfallChart
                            , pieChart = newPieChart}  , message <| HiddenRow -1)

            Animated isActive ->
              let
                cmd =
                  if isActive then
                    message <| StartAnimation model.height model.width
                  else
                    message <| HiddenRow -1
              in
                ( { model | settings = { oldChartSettings | animated = isActive } }, cmd )



            ------------------------------- chart's form option -------------------------

            -- for bar chart
            Stacked newBool ->
              let
                cmd =
                  if model.settings.animated then
                    message <| StartAnimation  model.height model.width
                  else
                    Cmd.none
              in
                ( { model | barChart = {oldBarChart | stacked = newBool } }, cmd )

            -- for curve chart
            InLine newBool ->
              let
                cmd =
                  if model.settings.animated then
                    message <| StartAnimation  model.height model.width
                  else
                    Cmd.none
              in
                ( { model | curveChart = { oldCurveChart |  inLine = newBool } }, cmd )


            HiddenRow row ->
              let
                exist = List.member row model.hiddenRows

                dataArray = Array.fromList model.data

                newRowList =
                  if row == -1 then
                    model.hiddenRows
                  else if exist then
                    List.Extra.remove row model.hiddenRows
                  else
                    model.hiddenRows ++ [row] |> List.sort

                newDisplayData =
                  model.data
                  |> List.indexedMap Tuple.pair
                  |> List.map (\(indexRow, rowList)-> if List.member indexRow newRowList then [] else rowList)
                  |> removeEmptyRow


                removeEmptyRow myList =
                  let
                    newList = List.Extra.remove [] myList

                  in
                    if List.member [] newList then
                      removeEmptyRow newList
                    else
                      newList
              in
              ( { model | hiddenRows =  newRowList, displayData = newDisplayData }, message <| UpdateDoubleChartData )





            InitData ->
              let
                dataArray = Array.fromList model.displayData
                lenData = List.length model.displayData

                -------------------------------------- area Chart
                sortRow =
                  Maybe.withDefault []
                  <| List.tail
                  <| List.map (\(x,y) ->x)
                  <| List.sortBy (Tuple.second)
                  <| List.indexedMap Tuple.pair
                  <| List.map maxNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) model.displayData)


                -- find the maximum value in the data list
                maxNumbFunc : List String -> Float
                maxNumbFunc sub_l =
                    sub_l
                        |> List.map (\v -> Maybe.withDefault 0 (String.toFloat v))
                        |> List.maximum
                        |> Maybe.withDefault 0

                maxS =
                    List.map maxNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) model.displayData)
                        |> List.maximum
                        |> Maybe.withDefault 0
                        |> (*) 1.2


                minNumbFunc : List String -> Float
                minNumbFunc sub_l =
                    sub_l
                        |> List.map (\v -> Maybe.withDefault 0 (String.toFloat v))
                        |> List.minimum
                        |> Maybe.withDefault 0

                minS =
                    List.map minNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) model.displayData)
                        |> List.minimum
                        |> Maybe.withDefault 0
                        |> (*) 1.2

                --------------------------------------------- double chart

                initIndexList =
                  if model.doubleChart.indexList == ([], []) then
                    ( List.map  String.fromInt <| List.range 0 (lenData - 2), [String.fromInt <| lenData - 1])
                  else
                    model.doubleChart.indexList

                getMin dataList =
                  List.map minNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) dataList)
                      |> List.minimum
                      |> Maybe.withDefault 0

                getMax dataList =
                  List.map maxNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) dataList)
                      |> List.maximum
                      |> Maybe.withDefault 0

                maxBarAxe = getMax model.doubleChart.dataInBar
                minBarAxe = getMin model.doubleChart.dataInBar
                maxCurveAxe = getMax model.doubleChart.dataInCurve
                minCurveAxe = getMin model.doubleChart.dataInCurve


                -- find le minimum val in waterfall chart
                waterFallData =
                  List.map (\row -> (.name <|  Maybe.withDefault (PrzTableCell "" "") <| Array.get 0 <| Array.fromList row
                  ,  .name <|  Maybe.withDefault (PrzTableCell "" "") <| Array.get (model.waterfallChart.colIndex + 1) <| Array.fromList row))
                  <| Maybe.withDefault []
                  <| List.tail model.data

                dataOnly =
                  waterFallData |> List.map (\(x,y)-> Maybe.withDefault 0 <| String.toFloat y )

                -- List accumuler
                cumulData : Int -> List Float ->List Float
                cumulData index dataList =
                  let
                    myArray = Array.fromList dataList
                    updatedArray =
                      myArray
                      |> Array.set index ( (Array.get index myArray |> Maybe.withDefault 0)
                                      + (Array.get (index-1) myArray |> Maybe.withDefault 0)
                                      )
                      |> Array.toList
                  in
                    if List.member index newSoldIndex then
                      cumulData (index + 1) dataList
                    else if index >= List.length dataList then
                      dataList
                    else
                      cumulData (index + 1) updatedArray


                soldIndexList : List Float -> Int ->  List Int -> List Int
                soldIndexList dataList index  result =
                  let
                    arrayVal = Array.fromList dataList
                    previousVal = Array.get (index - 1) arrayVal |> Maybe.withDefault 0
                    currentVal = Array.get (index) arrayVal |> Maybe.withDefault 0

                    updatedData =
                      if currentVal == previousVal then
                        let
                          isCorrect = isCorrectFlux dataList index
                        in
                          if isCorrect then
                            dataList
                          else
                            Array.set index (currentVal + previousVal) (Array.fromList dataList) |> Array.toList
                      else
                        Array.set index (currentVal + previousVal) (Array.fromList dataList) |> Array.toList

                    updatedResult =
                      if currentVal == previousVal then
                        let
                          isCorrect = isCorrectFlux dataList index
                        in
                        if isCorrect then
                          result ++ [index]
                        else
                          result
                      else
                        result
                  in
                    if 0 <= index && index < List.length dataList then
                      soldIndexList updatedData (index+1) updatedResult
                    else
                      updatedResult

                isCorrectFlux : List Float -> Int -> Bool
                isCorrectFlux dataList index=
                  let
                    array = Array.fromList dataList
                    previous = Array.get (index - 1) array |> Maybe.withDefault 0
                    current = Array.get (index) array |> Maybe.withDefault 0
                    endVal = Array.get ((List.length dataOnly) - 1) (Array.fromList dataOnly) |> Maybe.withDefault 0

                    updatedData =
                      if current == previous then
                        dataList
                      else
                        Array.set index (current + previous) (Array.fromList dataList) |> Array.toList
                  in
                    if index < List.length dataList then
                      isCorrectFlux updatedData (index+1)
                    else if (endVal == previous) then
                      True
                    else False

                cumulDataList = cumulData 1 dataOnly

                newSoldIndex =
                  if List.length model.waterfallChart.soldRows == 0 then
                    soldIndexList dataOnly 0 [0]
                  else
                    model.waterfallChart.soldRows

                waterfallMinHeight =
                  let
                    minData =
                      cumulDataList
                      |> List.minimum
                      |> Maybe.withDefault 0
                  in
                    if minData > 0 then
                      0
                    else
                      minData

                waterfallMaxHeight =
                    cumulDataList
                    |> List.maximum
                    |> Maybe.withDefault 0
                    |> (*)1.2

                initDisplayList =
                  if List.isEmpty model.displayData then
                    model.data
                  else
                    model.displayData

              in
                ( { model | data = model.data
                          , displayData = initDisplayList
                          , maxHeightScale = maxS
                          , minHeightScale = minS
                          , areaChart= { oldAreaChart | sortedData = sortRow}
                          , waterfallChart = { oldWaterfallChart | soldRows = newSoldIndex, maxHeight = waterfallMaxHeight, minHeight = waterfallMinHeight }
                          , doubleChart = { oldDoubleChart | indexList = initIndexList, maxBarHeight = maxBarAxe, minBarHeight = minBarAxe, maxCurveHeight = maxCurveAxe, minCurveHeight = minCurveAxe }}, Cmd.none )

            ChangeChart rowIndex typeChart ->
              let
                indexesList = model.doubleChart.indexList
                barChartIndex = List.map  (\i -> Maybe.withDefault 0 <| String.toInt i) <| Tuple.first indexesList
                curveChartIndex = List.map  (\i -> Maybe.withDefault 0 <| String.toInt i) <| Tuple.second indexesList
                updatedList =
                  if typeChart == "barChart" then
                    if List.member rowIndex barChartIndex then
                      indexesList
                    else
                      let
                        indexToRemove = getIndexOf rowIndex <| Tuple.second indexesList
                      in
                      (Tuple.first indexesList ++ [String.fromInt rowIndex], List.Extra.removeAt indexToRemove <| Tuple.second indexesList)
                  else
                    if List.member rowIndex curveChartIndex then
                      indexesList
                    else
                      let
                        indexToRemove = getIndexOf rowIndex <| Tuple.first indexesList
                      in
                      (List.Extra.removeAt indexToRemove <| Tuple.first indexesList,Tuple.second indexesList ++ [String.fromInt rowIndex])
              in
                ( { model | doubleChart = { oldDoubleChart | indexList = updatedList } }, message <| DropDownManager -1 )

            UpdateDoubleChartData ->
              let
                data = Array.fromList model.data
                indexesList = model.doubleChart.indexList
                barChartIndex =
                  List.map  (\i -> if List.member ((Maybe.withDefault 0 <| String.toInt i)) model.hiddenRows then -1 else (Maybe.withDefault 0 <| String.toInt i) )
                  <| Tuple.first indexesList

                curveChartIndex =
                  List.map  (\i ->  if List.member (Maybe.withDefault 0 <| String.toInt i)  model.hiddenRows then -1 else Maybe.withDefault 0 <| String.toInt i )
                  <| Tuple.second indexesList

                newBarChartData = List.map (\i -> Maybe.withDefault [] <| Array.get i data) <| removeEmptyRow barChartIndex
                newCurveChartData = List.map (\i -> Maybe.withDefault [] <| Array.get i data) <| removeEmptyRow curveChartIndex

                removeEmptyRow myList =
                  let
                    newList = List.Extra.remove -1 myList
                  in
                    if List.member -1 newList then
                      removeEmptyRow newList
                    else
                      newList
              in
                ( { model | doubleChart = { oldDoubleChart | dataInBar = newBarChartData, dataInCurve = newCurveChartData  } }, message <| InitData)



            SetPieChartColumn newCol ->
              let
                newPieChart = { oldPieChart | colName = Maybe.withDefault 0 <| String.toInt newCol}
              in
              ( { model | pieChart = newPieChart }, Cmd.none)

            SetWaterfallColumn newCol ->
              ( { model | waterfallChart = { oldWaterfallChart | colIndex = Maybe.withDefault 0 <| String.toInt newCol} }, Cmd.none)

            SetOrderAreaChart row newOrder ->
              let
                oldSortedRow = Array.fromList oldAreaChart.sortedData

                currentRow =
                  Maybe.withDefault 0 <| String.toInt row


                otherRow =
                  model.areaChart.sortedData
                  |> List.indexedMap Tuple.pair
                  |> List.map (\(index, val )-> if (String.toInt newOrder |> Maybe.withDefault 0 ) == val then (  String.fromInt index) else "")
                  |> String.join ""


                oldPos =
                  Array.get currentRow oldSortedRow |> Maybe.withDefault 0

                newPos =
                  Maybe.withDefault 0 <| String.toInt newOrder


                updatedNewIndex =
                  Array.set currentRow newPos oldSortedRow

                updatedOldIndex =
                  Array.set (String.toInt otherRow |> Maybe.withDefault 0) oldPos updatedNewIndex

                result = Array.toList updatedOldIndex


              in
              ( { model | areaChart = { oldAreaChart | sortedData = result} }, Cmd.none)
              -- ( { model | areaChart = oldAreaChart }, Cmd.none)



            SetFlux row newType ->
              let
                oldList = oldWaterfallChart.soldRows

                newSoldList =
                  if newType == "solde" then
                    if List.member row oldList then
                      oldList
                    else
                      oldList ++ [row]
                  else
                    if not (List.member row oldList) then
                      oldList
                    else
                      List.Extra.remove row oldList

              in
              ( { model | waterfallChart = { oldWaterfallChart | soldRows = newSoldList} }, Cmd.none)



            SetChartColor barType newColor ->
              let
                newWaterfallChart =
                  case barType of
                    "total bar" -> { oldWaterfallChart | totalBarColor = newColor}
                    "positive bar" -> { oldWaterfallChart | positiveBarColor = newColor}
                    "negative bar" -> { oldWaterfallChart | negativeBarColor = newColor}
                    "line" -> { oldWaterfallChart | lineColor = newColor}
                    _ -> { oldWaterfallChart | totalBarColor = newColor}
              in
              ( { model | waterfallChart = newWaterfallChart  }, Cmd.none)


            SetDotColor property newColor ->
              let
                newDotChart =
                  if property == "fillColor" then
                    { oldDotChart | fillColor = newColor }
                  else
                    { oldDotChart | borderColor = newColor }

              in
                ( { model | dotChart = newDotChart }, Cmd.none)





            --------------------------------- data update --------------------------------

            -- change color
            ChooseGraphLineColor index choosedColor ->
                let

                    -- _ = Debug.log "test = " index
                    newRows =
                        List.Extra.updateAt index (\row -> List.map (\cell -> { cell | color = choosedColor }) row) model.data
                in
                ( { model | data = newRows},  message <| UpdateDoubleChartData )


            -- reverse rows and columns
            TransposeTableData ->
                let
                    newRows =
                        List.Extra.transpose model.data
                in
                ( { model | data = newRows, hiddenRows= [], doubleChart = {oldDoubleChart | indexList = ([], [])} }, message <| HiddenRow -1)


            -- add new empty row
            AddNewCol ->
              ( { model | tableColumns = model.tableColumns+1 }, Cmd.none )


            -- add new empty column
            AddNewLine ->
              ( { model | tableRows = model.tableRows+1 }, Cmd.none )


            -- tmp input value
            TableCellTempValue i j str ->
              let
                -- filtre input value
                replace : String -> (Regex.Match -> String) -> String -> String
                replace userRegex replacer string =
                  case Regex.fromString userRegex of
                    Nothing ->
                      string
                    Just regex ->
                      Regex.replace regex replacer string

                delSpace : String -> String
                delSpace string =
                  replace "&nbsp;" (\_ -> "") string

                delBalise : String -> String
                delBalise string =
                  replace "<[/]?div>|<[/]?br>| [^\\w]" (\_ -> "") string

                val =
                  if  not (String.isEmpty str) then
                    delBalise <| delSpace str
                  else ""

                -- _ = Debug.log "tmp = "  val

              in
                ( { model | tmpTdValue = ( (i, j), val) }, Cmd.none )


            -- update, add, delete data
            UpdateChartTableData ( i, j ) ->
                let
                    -- check if it's a new row or column
                    -- _ = Debug.log "update  "  <|  String.fromInt i
                    isTheCorrectCell =
                      if Tuple.first model.tmpTdValue == (i, j) then True
                      else False
                    valIsEmpty = String.isEmpty <| Tuple.second model.tmpTdValue

                    -- _ = Debug.log "update  "  <|  model.data

                    nbOfRows = List.length model.data
                    nbOfColumns =
                        model.data
                            |> List.head
                            |> Maybe.withDefault []
                            |> List.length


                    isTheNextRow = if nbOfRows == i && j < nbOfColumns then True else False
                    isTheNextColumn = if nbOfColumns == j && i < nbOfRows then True else False

                    -- new row
                    addedRow = [ PrzTableCell "Header" "#866855" ] ++ List.repeat (nbOfColumns - 1) initialTableHeader
                    dataAfterAddNewRow =
                        model.data ++ [ addedRow ]

                    -- new column
                    addValueToRow : ( Int, List PrzTableCell ) -> List PrzTableCell
                    addValueToRow ( index, row ) =
                        if index == 0 then
                            row ++ [ PrzTableCell "Header" "#856666" ]
                        else
                            row ++ [ PrzTableCell "" "#000000" ]
                    dataAfterAddNewColumn =
                        List.map addValueToRow (List.indexedMap Tuple.pair model.data)

                    -- update row or column
                    updatedData =
                      if isTheNextRow then
                        dataAfterAddNewRow
                      else
                        dataAfterAddNewColumn

                    updatedDoubleChartIndexList =
                      if isTheNextRow && isTheCorrectCell && not valIsEmpty then
                        (Tuple.first model.doubleChart.indexList ++[ String.fromInt i],Tuple.second model.doubleChart.indexList )
                      else
                        model.doubleChart.indexList

                    -- Update the data list
                    mydata =
                        Array2D.fromList (if (isTheNextRow || isTheNextColumn) && isTheCorrectCell && not valIsEmpty then
                                            updatedData
                                          else model.data
                                          )

                    oldCell =
                        Array2D.get i j mydata
                            |> Maybe.withDefault (PrzTableCell "" "")


                    -- update cell
                    dataAfterUpdate =
                        if isTheCorrectCell then
                            Array2D.set i j { oldCell | name = Tuple.second model.tmpTdValue } mydata
                        else
                          mydata

                    -- convert array to list
                    newdataList =
                          dataAfterUpdate.data |> Array.toList |> List.map Array.toList

                    -- delete empty row
                    indexOfEmptyRow =
                       String.concat
                       <| List.map (\(index,bool) -> if bool == 1 then String.fromInt index else "")
                       <| List.indexedMap Tuple.pair
                       <| List.map (\x -> if String.contains "0" x then 0 else 1)
                       <| List.map  String.concat
                       <| List.map (\ii -> List.map (\jj -> if jj.name == "" then "1" else "0") ii ) newdataList

                    deleteEmptyRow =
                      if not (String.isEmpty indexOfEmptyRow) then
                        List.map (\x-> (Maybe.withDefault []
                                        <| Array.get x (Array.fromList newdataList)) )
                                        <| List.range 0
                                        <| (Maybe.withDefault 0
                                        <| String.toInt indexOfEmptyRow) - 1
                      else
                        newdataList

                    -- delete empty column
                    checkArray = Array2D.fromList deleteEmptyRow

                    getEmptyCells = Array2D.fromList <| List.map (\ii -> List.map (\jj -> if jj.name == "" then "1" else "0") ii ) newdataList

                    indexOfEmptyCol =
                         String.concat
                         <| List.map (\(index,bool) -> if bool == 1 then String.fromInt index else "")
                         <| List.indexedMap Tuple.pair
                         <| List.map (\x -> if String.contains "0" x then 0 else 1)
                         <| List.map  String.concat
                         <| List.map (\col -> List.map (\row -> Maybe.withDefault "" <| Array2D.get row (Tuple.first col) getEmptyCells) (Tuple.second col)  )
                         <| List.indexedMap Tuple.pair
                         <| List.map (\(jj) -> List.map (\ii ->  ii)(List.range 0 (nbOfRows - 1)))
                         <| List.range 0 (nbOfColumns - 1)

                    deleteEmptyCol =
                      if not (String.isEmpty indexOfEmptyCol) then
                        List.map (\(row, rowList) -> List.map (\col -> Maybe.withDefault (PrzTableCell "" "") <| Array2D.get row col checkArray)  rowList )
                        <| List.indexedMap Tuple.pair
                        <| List.map (\(jj) -> List.map(\ii ->  ii) (List.range 0 ( (Maybe.withDefault 0 <| String.toInt indexOfEmptyCol) - 1) ) )
                        <| List.range 0 (nbOfRows - 1)
                      else
                        deleteEmptyRow

                    -- find the maximum value in the data list
                    maxNumbFunc : List String -> Float
                    maxNumbFunc sub_l =
                        sub_l
                            |> List.map (\v -> Maybe.withDefault 0 (String.toFloat v))
                            |> List.maximum
                            |> Maybe.withDefault 0

                    maxS =
                        List.map maxNumbFunc (List.map (\r -> List.map (\obj -> obj.name) r) newdataList)
                            |> List.maximum
                            |> Maybe.withDefault 0
                            |> (*) 1.2
                in
                  ( { model | data = deleteEmptyCol, maxHeightScale = maxS, tmpTdValue= ( ( -1, -1 ), "" ), doubleChart = { oldDoubleChart | indexList = updatedDoubleChartIndexList} }, message <| HiddenRow -1 )


            ------------------------- Chart settings update ------------------------------

            -- main style

            SetGlobalFontFamily newFontFamily ->
                ( {  model | settings = { oldChartSettings | globalFontFamily = newFontFamily } }, Cmd.none )

            SetChartBackground newColor ->
                ( {  model | settings = { oldChartSettings | backgroundColor = newColor } }, Cmd.none )


            -- header style

            SetTitleVisibility bool ->
                ( {  model | settings = { oldChartSettings | showTitle = bool } }, Cmd.none )

            SetSourceVisibility bool ->
                ( {  model | settings = { oldChartSettings | showSource = bool } }, Cmd.none )

            SetChartTitle newTitle ->
                ( {  model | settings = { oldChartSettings | title = newTitle } }, Cmd.none )

            SetChartSource newSource ->
                ( {  model | settings = { oldChartSettings | source = newSource } }, Cmd.none )

            SetTitlePos newPos ->
                let
                    titlesTop = String.contains "top" newPos

                    sourcePosY =
                      case (String.contains "top" model.settings.sourcePosition) of
                        True ->
                          if titlesTop then
                            toFloat <| 40 + 30
                          else
                            toFloat <| 30
                        False ->
                          if not titlesTop then
                            toFloat <| (if legendIsMiddle  then round model.height + 40 else round model.height + 40) + 40
                          else
                            toFloat <| (if legendIsMiddle  then round model.height + 40 else round model.height + 40) + 40

                    updatedSourcePos = (Tuple.first model.settings.sourcePos, sourcePosY)

                    newTitlePos = getTitlePosition model newPos

                in
                ( {  model | settings = { oldChartSettings | titlesPos = newTitlePos, titlePosition = newPos, sourcePos = updatedSourcePos } }, Cmd.none )

            SetSourcePos newPos ->
              let

                sourceTop =  String.contains "top" newPos

                titlePosY =
                  case (String.contains "top" model.settings.titlePosition) of
                    True ->
                        toFloat <| 40
                    False ->
                      if sourceTop then
                        toFloat <| 20 + (if legendIsMiddle  then round model.height + 60 else round model.height + 70) - 15
                      else
                        toFloat <| (if legendIsMiddle  then round model.height + 40 else round model.height + 40) + 10

                updateTitlePos = (Tuple.first model.settings.titlesPos, titlePosY)

                newSourcePosition = getSourcePosition model newPos

              in
                ( {  model | settings = { oldChartSettings | sourcePos = newSourcePosition, sourcePosition = newPos, titlesPos = updateTitlePos } }, Cmd.none )

            SetTxtAttributes champs attribute newValue ->
              let
                 -- _ = Debug.log "test = " setTitle.fontSize
                 setTitle = model.settings.titleTxtSettings
                 setSource = model.settings.sourceTxtSettings
                 setLegend = model.settings.legendTxtSettings
                 setData = model.settings.valueTxtSettings
                 setAxeX = model.settings.axeXTxtSettings
                 setAxeY = model.settings.axeYTxtSettings

                 settingsUpdated =
                   case champs of
                     "title" ->
                              case attribute of
                                "fontFamily" -> {oldChartSettings | titleTxtSettings = { setTitle | fontFamily = newValue}}
                                "fontSize" -> {oldChartSettings | titleTxtSettings = { setTitle |fontSize = (String.toInt newValue |> Maybe.withDefault 14)}}
                                "fontColor" -> {oldChartSettings | titleTxtSettings = { setTitle |fontColor = newValue}}
                                _ -> {oldChartSettings | titleTxtSettings = setTitle}
                     "source" ->
                              case attribute of
                                "fontFamily" -> {oldChartSettings | sourceTxtSettings = { setSource | fontFamily = newValue}}
                                "fontSize" -> {oldChartSettings | sourceTxtSettings = { setSource |fontSize = (String.toInt newValue |> Maybe.withDefault 14)}}
                                "fontColor" -> {oldChartSettings | sourceTxtSettings = { setSource |fontColor = newValue}}
                                _ -> {oldChartSettings | titleTxtSettings = setTitle}
                     "legend" ->
                              case attribute of
                                "fontFamily" -> {oldChartSettings | legendTxtSettings = { setLegend | fontFamily = newValue}}
                                "fontSize" -> {oldChartSettings | legendTxtSettings = { setLegend |fontSize = (String.toInt newValue |> Maybe.withDefault 14)}}
                                "fontColor" -> {oldChartSettings | legendTxtSettings = { setLegend |fontColor = newValue}}
                                _ -> {oldChartSettings | titleTxtSettings = setTitle}
                     "data-value" ->
                              case attribute of
                                "fontFamily" -> {oldChartSettings | valueTxtSettings = { setData | fontFamily = newValue}}
                                "fontSize" -> {oldChartSettings | valueTxtSettings = { setData |fontSize = (String.toInt newValue |> Maybe.withDefault 14)}}
                                "fontColor" -> {oldChartSettings | valueTxtSettings = { setData |fontColor = newValue}}
                                _ -> {oldChartSettings | titleTxtSettings = setTitle}
                     "axe-x" ->
                             case attribute of
                               "fontFamily" -> {oldChartSettings | axeXTxtSettings = { setAxeX | fontFamily = newValue}}
                               "fontSize" -> {oldChartSettings | axeXTxtSettings = { setAxeX |fontSize = (String.toInt newValue |> Maybe.withDefault 11)}}
                               "fontColor" -> {oldChartSettings | axeXTxtSettings = { setAxeX |fontColor = newValue}}
                               _ -> {oldChartSettings | titleTxtSettings = setTitle}
                     "axe-y" ->
                             case attribute of
                               "fontFamily" -> {oldChartSettings | axeYTxtSettings = { setAxeY | fontFamily = newValue}}
                               "fontSize" -> {oldChartSettings | axeYTxtSettings = { setAxeY |fontSize = (String.toInt newValue |> Maybe.withDefault 11)}}
                               "fontColor" -> {oldChartSettings | axeYTxtSettings = { setAxeY |fontColor = newValue}}
                               _ -> {oldChartSettings | titleTxtSettings = setTitle}
                     _ -> {oldChartSettings | titleTxtSettings = setTitle}

              in
                ( {  model | settings = settingsUpdated }, Cmd.none )


            -- Axes settings

            SetAxesVisibility bool ->
                ( {  model | settings = { oldChartSettings | showAxes = bool } }, Cmd.none )

            SetAxeX newTitle ->
                ( {  model | settings = { oldChartSettings | axeX = newTitle } }, Cmd.none )

            SetAxeY newTitle ->
                ( {  model | settings = { oldChartSettings | axeY = newTitle } }, Cmd.none )


            -- data value settings

            SetValueVisibility bool ->
                ( {  model | settings = { oldChartSettings | showValueOnHover = bool } }, Cmd.none )

            SetSimpleData newBool ->
                ( {  model | settings = { oldChartSettings | simpleData = newBool} }, Cmd.none )

            SetDecimalSeparator newSep ->
              let
                s =
                  if newSep == oldChartSettings.thousandsSeparator then
                    case newSep of
                      "." -> (",", ".")
                      "," -> (".", ",")
                      _ -> (",", ".")
                  else (oldChartSettings.thousandsSeparator, newSep)

              in
                ( {  model | settings = { oldChartSettings | decimalSeparator = Tuple.second s, thousandsSeparator = Tuple.first s } }, Cmd.none )

            SetThousandsSeparator newSep ->
              let
                s =
                  if newSep == oldChartSettings.decimalSeparator then
                    case newSep of
                      "," -> (",", ".")
                      "." -> (".", ",")
                      _ -> (",", ".")
                  else (newSep, oldChartSettings.decimalSeparator)
              in
                ( {  model | settings = { oldChartSettings | decimalSeparator = Tuple.second s, thousandsSeparator = Tuple.first s } }, Cmd.none )

            SetDataPrefix newPrefix ->
              let
                s = if (String.length newPrefix) > 1 then ("", oldChartSettings.dataSuffix)
                    else (newPrefix, "")
              in
                ( {  model | settings = { oldChartSettings | dataPrefix = Tuple.first s, dataSuffix = Tuple.second s } }, Cmd.none )

            SetDataSuffix newSuffix ->
              let
                s = if (String.length newSuffix) > 1 then (oldChartSettings.dataPrefix, "")
                    else ("", newSuffix)
              in
                ( {  model | settings = { oldChartSettings | dataPrefix = Tuple.first s, dataSuffix = Tuple.second s } }, Cmd.none )


            -- legend settings

            SetShowLegend bool ->
                ( {  model | settings = { oldChartSettings | showLegend = bool } }, Cmd.none )

            SetAutoColorForLegend bool ->
                let
                  oldLegendTxt = oldChartSettings.legendTxtSettings
                in
                  ( {  model | settings = { oldChartSettings | legendTxtSettings = { oldLegendTxt | autoColor = bool } } }, Cmd.none )

            SetLegendPos newPos ->
                ( {  model | settings = { oldChartSettings | legendPosition = newPos } }, Cmd.none )




------------------------------------ utils functions ---------------------------------



getTitlePosition : ChartModel -> String -> (Float, Float)
getTitlePosition model newPos =
  let
    posX = getHeaderPosX model newPos

    titlesTop =
        String.contains "top" newPos

    posY =
        case titlesTop of
            True -> toFloat <| 40
            False ->
              if String.contains "top" model.settings.sourcePosition then
                toFloat <| 20 + (if String.contains "middle" model.settings.legendPosition then round model.height + 60 else round model.height + 70) - 15
              else
                toFloat <| (if String.contains "middle" model.settings.legendPosition then round model.height + 40 else round model.height + 40) + 10
  in
    ( toFloat posX, posY )


getSourcePosition : ChartModel -> String -> (Float, Float)
getSourcePosition model newPos =
  let
    posX = getHeaderPosX model newPos

    sourceTop =
        String.contains "top" newPos

    posY =
        case sourceTop of
            True ->
              if String.contains "top" model.settings.titlePosition then
                toFloat <| 40 + 30
              else
                toFloat <| 30

            False ->
              if String.contains "bottom" model.settings.titlePosition then
                toFloat <| (if String.contains "middle" model.settings.legendPosition then round model.height + 40 else round model.height + 40) + 40
              else
                toFloat <| (if String.contains "middle" model.settings.legendPosition then round model.height + 40 else round model.height + 40) + 40
  in
    ( toFloat posX, posY )

getHeaderPosX : ChartModel -> String -> Int
getHeaderPosX  model newPos  =
  let
    getPosX : Int -> Int
    getPosX x =
        40 + ((round model.width - (40 * 2)) // 3) * x
  in
    case newPos of
        "bottom_left" -> getPosX 0
        "top_left" -> getPosX 0
        "top_center" -> round model.width // 2
        "bottom_center" -> round model.width // 2
        "top_right" -> round (model.width - 40)
        "bottom_right" -> round (model.width - 40)
        _ -> getPosX 0
