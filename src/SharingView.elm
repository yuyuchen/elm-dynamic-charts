module SharingView exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onMouseUp, onBlur, onInput, onCheck, keyCode, on)
import Html.Events.Extra exposing (onChange)
import Axis
import Path exposing (Path)
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, text_, svg)
import TypedSvg.Attributes exposing (class, textAnchor, dominantBaseline, transform, viewBox, fill, stroke)
import TypedSvg.Attributes.InPx exposing (x, y, strokeWidth)
import TypedSvg.Events exposing (onLoad)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (AnchorAlignment(..), DominantBaseline(..), Transform(..), Paint(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)
import Json.Decode as Decode exposing (at)
import Json.Decode as Json
import List.Extra exposing (uncons)
import Array

-- Prezance packages --
import Update exposing (Msg(..))
import Model exposing (PrzTableCell, initialCell, initialTableHeader, ChartSettingsModel, fontFamilyList, ChartModel)
import SharingStyle exposing (svgChartStyle)
import Util exposing
  ( getLegendPosition
  , formatData
  , transformToLineData
  , tranfromToAreaData
  , circle
  )



-- data tabs content view
dataTabView : ChartModel -> Html Msg
dataTabView model =
  div [ Html.Attributes.class "data-tab-content" ]
      [ div [Html.Attributes.class "btn-load-data", onClick <| DropDownManager -1]
            [ button [] [ img [src "../assets/icones-svg/settings/link-icone.svg"][],  Html.text "Données existantes"]
            , button [] [ img [src "../assets/icones-svg/settings/import-icone.svg"][], Html.text "Charger un fichier"]
            ]
      , div [ Html.Attributes.class "table-data" ]
            [
              tableView model
            ]
      ]


-- settings tabs content view
settingsTabView : ChartModel -> { name : String, stack : Bool, isLine : Bool} -> Html Msg
settingsTabView model cm =
  div [ Html.Attributes.class "settings-chart-content" ]
      [ mainStyleSettingsView model {myChart = cm.name, stacked = cm.stack, inLine = cm.isLine}
      , titleSettingsView model.settings
      , axeSettingsView model.settings
      , if model.chartsTabs == 4 then g[][] else dataSettingsView model.settings
      , legendSettingsView model.settings
      ]




------------------------------ Curve Chart ----------------------------------------


dataOnHover : ChartModel -> (Float, (Float, Float)) ->  Svg Msg
dataOnHover model (val, (posX, posY)) =
  let
    formattedData = Tuple.first <| formatData (String.fromFloat val) model.settings
    simpleData = Tuple.second <| formatData (String.fromFloat val) model.settings

    (defaultVal, simpleVal) =
      if String.contains "-" (String.fromFloat val) then
        ("-"++formattedData, "-"++simpleData)
      else
        (formattedData ,simpleData)

  in
    g [ TypedSvg.Attributes.class [ "point" ++ String.replace "." ""  (String.fromFloat (val)) ] ]
      [
        Path.element circle [ fill (Paint Color.white), stroke (Paint Color.black), transform [ Translate posX posY ]]
      , text_
        [ x <| posX - 5
        , y <| posY - 10
        -- , textAnchor AnchorMiddle
        , TypedSvg.Attributes.class [ "data-value" ]
        ]
        [ TypedSvg.Core.text  <| if model.settings.simpleData && simpleData /= "" then simpleVal else defaultVal ]
      ]


drawLine :  ChartModel -> (String, List (String, Float)) ->  Svg Msg
drawLine model (color, list)  =
  let
     c = hexToColor color |> Result.withDefault Color.gray
     line : List ( String, Float ) -> Path
     line lineData =
       let
         inLine = model.curveChart.inLine
       in
         List.map (transformToLineData model) lineData
             |>  if inLine then
                   Shape.line Shape.linearCurve
                 else
                   Shape.line Shape.monotoneInXCurve
  in
    Path.element (line list) [ stroke <| Paint <| c, strokeWidth 3, fill <| PaintNone ]



drawArea :  ChartModel -> (String, List (String, Float)) ->  Svg Msg
drawArea model (color, list)  =
  let
     c = hexToColor color |> Result.withDefault Color.gray
     area : List ( String, Float ) -> Path
     area data =
       let
         inLine = model.curveChart.inLine
       in
       List.map (tranfromToAreaData model) data
          |>  if inLine then
                Shape.area Shape.linearCurve
              else
                Shape.area Shape.monotoneInXCurve
  in
    Path.element (area list) [ strokeWidth 3, fill <| Paint <| c]

---------------------------- Chart Options ---------------------------------------------------

-- for Curve Chart
lineOption : {inLine : Bool } -> Html Msg
lineOption op =
  div [ Html.Attributes.class "two-col" ]
      [ div [Html.Attributes.class "option-name"]
            [ span [] [ Html.text "Courbe en ligne" ]]
      , div [Html.Attributes.class "checkbox"]
            [
              input [ type_ "checkbox", id "line"
                    , onCheck <| InLine
                    , checked op.inLine
                    ] []
            , label [for "line"][]
            ]
      ]


-- for Bar Chart
stackedOption : {stacked : Bool} -> Html Msg
stackedOption op =
  div [ Html.Attributes.class "two-col" ] ---------- stack mode
      [ div [Html.Attributes.class "option-name"]
            [ span [] [ Html.text "Mode empilement" ]]
      , div [Html.Attributes.class "checkbox"]
            [
              input [ type_ "checkbox", id "stack"
                    , onCheck <| Stacked
                    , checked op.stacked
                    ] []
            , label [for "stack"][]
            ]
      ]



------------------------------- table data view -------------------------------------

tableView : ChartModel -> Html Msg
tableView cm =
    let
        minCol = cm.tableColumns
        minRows = cm.tableRows
        nbOfRows =  List.length cm.data
        abcList = Array.fromList ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        nbOfColumns =
            cm.data
                |> List.head
                |> Maybe.withDefault []
                |> List.length

        row = List.repeat (nbOfColumns) initialCell

        addValueToColumns : ( Int, List PrzTableCell ) -> List PrzTableCell
        addValueToColumns ( i, r ) = r ++ List.repeat (minCol - nbOfColumns +1) initialCell

        abcHeader = List.map  (\x -> (PrzTableCell (Maybe.withDefault "A" <| Array.get x abcList) "#FFFFFF" ))   (List.range 0 (if nbOfColumns > minCol then nbOfColumns else minCol))
        addedRows = cm.data ++ List.repeat (minRows - nbOfRows) row
        addedEmptyLine = addedRows ++ List.repeat 1 row
        addedColumns =  List.map addValueToColumns (List.indexedMap Tuple.pair addedEmptyLine)

        newData =
            List.append [ abcHeader ] addedColumns

        tableBody =
            let
                rows =
                    newData
                        |> List.map List.Extra.uncons
                        |> List.map (Maybe.withDefault ( initialTableHeader, [] ))
                        |> List.indexedMap Tuple.pair
            in
            List.map (addColorOnRow cm ) rows
    in
    div []
        [ table [] tableBody ]


addColorOnRow : ChartModel -> ( Int, ( PrzTableCell, List PrzTableCell ) ) -> Html Msg
addColorOnRow model ( i, item ) =
    let
        abcList =  ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

        innerHtmlDecoder = Decode.at [ "target", "innerHTML" ] Decode.string

        onKeyDown : (Int -> msg) -> Attribute msg
        onKeyDown tagger =
          on "keydown" (Json.map tagger keyCode)

        -- _ = Debug.log "essst == " (Decode.map (TableCellTempValue (1) (1)) innerHtmlDecoder)

        rowTail = Tuple.second item |> List.indexedMap Tuple.pair

        lastRow = model.tableRows +1
        lastCol = model.tableColumns - 1

        editableCell : ( Int, PrzTableCell ) -> Html Msg
        editableCell ( j, cellule ) =
            td [onClick <| DropDownManager -1]
                [
                  if j == lastCol  && i == 0 then
                    button[onClick AddNewCol][Html.text "+" ]
                  else if j == lastCol || i == lastRow then
                    div [] []
                  else
                    div
                        [
                          contenteditable <| not (List.member cellule.name abcList)
                        , Html.Events.on "input" (Decode.map (TableCellTempValue (i - 1) (j + 1)) innerHtmlDecoder)
                        , onKeyDown <| KeyDown (i - 1) (j + 1)
                        , onMouseUp <| TableCellTempValue (i - 1) (j + 1) cellule.name
                        , onBlur <| UpdateChartTableData ( i - 1, j + 1 )
                        ]
                        [ Html.text cellule.name ]
                ]
    in
    tr []
        (
        [ if i == 0 then -- interpose icone
            td
              [ Html.Attributes.style "Html.text-align" "center"
              , onClick TransposeTableData
              ]
              [ img [src "../assets/icones-svg/settings/interpose-icone.svg"][] ]

          else if i == lastRow then -- button + new row
            td
              [onClick <| DropDownManager -1]
              [ button[onClick AddNewLine ][Html.text "+" ] ]
          else -- count row && add color bubble
            td [Html.Attributes.class ( if String.isEmpty (Tuple.first item).name then "no-data" else "line-header"), onClick <| DropDownManager -1]
                [ span [] [ Html.text <| if i == lastRow then "" else String.fromInt i]
                , if (i == 1 || model.chartsTabs == 6) then
                    span [] []
                  else
                    div [Html.Attributes.class "color-bubble-container"]
                      [
                       input
                          [ type_ "color"
                          , Html.Attributes.class "color-bubble"
                          , onInput <| ChooseGraphLineColor (i-1)
                          , value (Tuple.first item).color
                          ]
                          []
                      ]
                ]
         , if ( i == 1 && model.chartsTabs == 7) then ---------------------------------  double chart
             td [ onClick <| DropDownManager -1
                , Html.Attributes.style "background" "#F2F2F2"
                , Html.Attributes.style "border" "White 1px solid"
                ][]
           else if ( i == 0 && model.chartsTabs == 7) then -- column's title
             td [ Html.Attributes.style "background" "#FD738F"
                , Html.Attributes.style "color" "white"
                , onClick <| DropDownManager -1][text "Type"]
           else if (2 <= i && i <= List.length model.data && model.chartsTabs == 7) -- add chart option
             then
               let
                 active = if Tuple.first model.tableDropdown == i then Tuple.second model.tableDropdown else False
                 atrr = if active then Html.Attributes.style "display" "list-item" else Html.Attributes.style "display" "none"

                 barChartData = Tuple.first model.doubleChart.indexList
                 curveChartData = Tuple.second model.doubleChart.indexList

                 isBarChart = List.member( String.fromInt (i - 1)) barChartData
                 isCurveChart = List.member( String.fromInt (i - 1)) curveChartData
               in
                 td [ Html.Attributes.style "overflow" "inherit"
                    , Html.Attributes.style "position" "relative"
                    , Html.Attributes.style "background" "#F2F2F2"
                    , Html.Attributes.style "border" "White 1px solid"
                    ]
                       [ ul []
                            [ li [ onClick <| DropDownManager i]
                                 [ img [ if isBarChart then
                                          src "../assets/icones-svg/charts-navbar/Gris/bar-chart.svg"
                                         else
                                           src "../assets/icones-svg/charts-navbar/Gris/curve-chart.svg"
                                       , width 24
                                       , height 20
                                       -- , Html.Attributes.style "height" "20"
                                       ] []
                                 , img [ src "../assets/icones-svg/settings/fleche-gris.svg" ] []
                                 ]
                            , li [atrr] [ ul []
                                          [ li [onClick <| ChangeChart (i - 1) "barChart"] [ img [ src "../assets/icones-svg/charts-navbar/Gris/bar-chart.svg" ][]]
                                          , li [onClick <| ChangeChart (i - 1) "curveChart"] [ img [ src "../assets/icones-svg/charts-navbar/Gris/curve-chart.svg" ][]]
                                          ]

                                    ]
                            ]
                       ]
           else
             td [ Html.Attributes.style "display" <| if model.chartsTabs == 7 then "revert" else "none"
                , Html.Attributes.style "background" "#F2F2F2", Html.Attributes.style "border" "White 1px solid"
                , onClick <| DropDownManager -1
                ][]
         , if ( i == 1 && model.chartsTabs == 6) then ---------------------------------  waterfall chart
             td [ Html.Attributes.style "background" "#F2F2F2"
                , Html.Attributes.style "border" "White 1px solid"
                , onClick <| DropDownManager -1
                ][]
           else if ( i == 0 && model.chartsTabs == 6) then -- column's title
             td [ Html.Attributes.style "background" "#FD738F"
                , onClick <| DropDownManager -1
                , Html.Attributes.style "color" "white"][text "Type"]
           else if (2 <= i && i <= List.length model.data && model.chartsTabs == 6) -- add chart option
             then
               let
                active = if Tuple.first model.tableDropdown == i then Tuple.second model.tableDropdown else False
                attribut = if active then Html.Attributes.style "display" "list-item" else Html.Attributes.style "display" "none"
                sold =
                  if List.member (i-2) model.waterfallChart.soldRows then
                    True
                  else
                    False
               in
               td [ Html.Attributes.style "overflow" "inherit"
                  , Html.Attributes.style "position" "relative"
                  , Html.Attributes.style "background" "#F2F2F2"
                  , Html.Attributes.style "border" "White 1px solid"
                  ]
                     [ ul []
                          [ li [ onClick <| DropDownManager i]
                               [ img [ if sold then
                                        src "../assets/icones-svg/dropdown/waterfall-solde.svg"
                                       else
                                         src "../assets/icones-svg/dropdown/waterfall-flux.svg"
                                     ] []
                               , img [ src "../assets/icones-svg/settings/fleche-gris.svg" ] []
                               ]
                          , li [attribut] [ ul []
                                        [ li [onClick <| SetFlux (i-2) "flux"] [ img [ src "../assets/icones-svg/dropdown/waterfall-flux.svg" ][]]
                                        , li [onClick <| SetFlux (i-2) "solde"] [ img [ src "../assets/icones-svg/dropdown/waterfall-solde.svg" ][]]
                                        ]

                                  ]
                          ]
                     ]
           else
             td [ Html.Attributes.style "display" <| if model.chartsTabs == 6 then "revert" else "none"
                , Html.Attributes.style "background" "#F2F2F2"
                , Html.Attributes.style "border" "White 1px solid"
                , onClick <| DropDownManager -1
                ][]
         , td [ onClick <| DropDownManager -1 ]
            [
              div
                [ contenteditable <| if (i == 0 || lastRow == i) then False else True
                , Html.Events.on "input" (Decode.map (TableCellTempValue (i - 1) 0) innerHtmlDecoder)
                , onMouseUp <| TableCellTempValue (i - 1) 0 <| (Tuple.first item).name
                , onBlur <| UpdateChartTableData ( i - 1, 0 )
                , onKeyDown <| KeyDown (i - 1) 0
                ]
                [ Html.text <| (Tuple.first item).name ]
            ]


         ]
         ++List.map (\( j, cell ) -> editableCell ( j, cell )) rowTail
        )




-------------------------------- Legend View --------------------------------

legendView : ChartModel -> Svg Msg
legendView model =
  let
      colors = Array.fromList <| getLinesColor model.data

      funcPalette : ( Int, List PrzTableCell ) -> Svg Msg
      funcPalette ( i, slist ) =
          if i == 0 then g [] []
          else
              let


                  -- _ = Debug.log "ok" <| (String.fromInt i) ++ "  " ++ legName.name

                  legName =
                    List.head slist |> Maybe.withDefault (PrzTableCell "" "")

                  autoColor =
                    model.settings.legendTxtSettings.autoColor

                  legendPos =
                    getLegendPosition model i

                  legendSize =
                    toFloat  model.settings.legendTxtSettings.fontSize

                  rectColor =
                    hexToColor ( Maybe.withDefault "" <| Array.get (i - 1) colors) |> Result.withDefault Color.gray

                  backgroundColor =
                    hexToColor  model.settings.backgroundColor |> Result.withDefault Color.gray
              in
                g [ transform [ Translate (Tuple.first legendPos) (Tuple.second legendPos) ]
                  , TypedSvg.Attributes.visibility <| if model.settings.showLegend then "visible" else "hidden"
                  , onClick <| HiddenRow i
                  , TypedSvg.Attributes.class ["legend-item"]
                  ]
                  [ rect [
                           -- TypedSvg.Attributes.class [ "palette" ++ String.fromInt i ]
                          TypedSvg.Attributes.InPx.width legendSize
                         , TypedSvg.Attributes.InPx.height legendSize
                         , fill <| if List.member i model.hiddenRows then Paint backgroundColor else Paint rectColor
                         , stroke <| Paint rectColor
                         ]
                         []
                  , text_ [ x 24
                          , textAnchor AnchorStart
                          , dominantBaseline DominantBaselineHanging
                          , TypedSvg.Attributes.style (if autoColor then ("fill :" ++ legName.color ) else "")
                          ]
                          [ TypedSvg.Core.text legName.name ]
                  ]
  in
    g [TypedSvg.Attributes.class ["svg-chart-legend"]]
      (List.map funcPalette (List.indexedMap Tuple.pair model.data))


getLinesColor : List (List PrzTableCell) -> List String
getLinesColor data =
  List.map  .color
  <| List.map (\r -> Maybe.withDefault (PrzTableCell "" "") <| List.head r)
  <| Maybe.withDefault []
  <| List.tail data



------------------------------- sub settings view --------------------------------

mainStyleSettingsView : ChartModel -> {myChart: String, stacked: Bool, inLine : Bool}-> Html Msg
mainStyleSettingsView model option =
  let
    waterFallStyle = Html.Attributes.style "display" <| if model.chartsTabs == 6 then "flex" else "none"
    dotChartStyle = Html.Attributes.style "display" <| if model.chartsTabs == 5 then "flex" else "none"

  in
  div []
      [ div [Html.Attributes.class "title"]
            [ img [src "../assets/icones-svg/settings/fleche-icone-1.svg"][]
            , h3 [][Html.text "Style"]
            ]
      , div [ Html.Attributes.class "style-options"
            ]
            [ div [ Html.Attributes.class "two-col" ] ---------- checkbox for animation
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Activer les animations" ]]
                , div [Html.Attributes.class "checkbox"]
                      [
                        input [ type_ "checkbox", id "animation"
                              , onCheck <| Animated
                              , checked model.settings.animated
                              ] []
                      , label [for "animation"][]
                      ]
                ]
            , span [Html.Attributes.class "separation-barre"] []
            , if option.myChart == "barChart" || option.myChart == "horizontalBarChart"  || option.myChart == "doubleChart" then
                stackedOption {stacked = option.stacked}
              else if option.myChart == "curveChart" || option.myChart == "doubleChart" || option.myChart == "areaChart"then
                lineOption { inLine = option.inLine }
              else
                span [][]
            , span [Html.Attributes.class <| if option.myChart == "doubleChart" || option.myChart == "barChart" || option.myChart == "curveChart" || option.myChart == "horizontalBarChart" then "separation-barre" else ""] []
            , div [ Html.Attributes.class "two-col" ]---------- global font family
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Police globale" ]]
                , div []
                      [
                          select [onChange <| SetGlobalFontFamily]
                         (List.map (fontFamilyOptions model.settings "") <| "-- Police --" :: ( List.sort fontFamilyList) )
                      ]
                ]
            , span [Html.Attributes.class "separation-barre"] []
            , div [ Html.Attributes.class "two-col" ] ---------- background color
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Couleur d'arrière plan" ]]
                , div [Html.Attributes.class "color-picker"]
                      [ input [ type_ "color"
                              , onInput <| SetChartBackground
                              , value model.settings.backgroundColor
                              ][]
                      ]
                ]

            -------------------------   dot Chart
            , span [Html.Attributes.class "separation-barre", dotChartStyle] []
            , div [ Html.Attributes.class "two-col" , dotChartStyle] ---------- negative color
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Remplissage des points" ]]
                , div [Html.Attributes.class "color-picker"]
                      [ input [ type_ "color"
                              , onInput <| SetDotColor "fillColor"
                              , value model.dotChart.fillColor
                              ][]
                      ]
                ]
            , span [Html.Attributes.class "separation-barre", dotChartStyle] []
            , div [ Html.Attributes.class "two-col" , dotChartStyle] ---------- line color
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Contour des points" ]]
                , div [Html.Attributes.class "color-picker"]
                      [ input [ type_ "color"
                              , onInput <| SetDotColor "borderColor"
                              , value model.dotChart.borderColor
                              ][]
                      ]
                ]




            -----------------  for waterfall chart
            , span [ Html.Attributes.class "separation-barre", waterFallStyle ] []
            , div [ Html.Attributes.class "two-col" , waterFallStyle] ---------- total color
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Couleur des soldes" ]]
                , div [Html.Attributes.class "color-picker"]
                      [ input [ type_ "color"
                              , onInput <| SetChartColor "total bar"
                              , value model.waterfallChart.totalBarColor
                              ][]
                      ]
                ]
            , span [Html.Attributes.class "separation-barre", waterFallStyle] []
            , div [ Html.Attributes.class "two-col", waterFallStyle ] ---------- positive color
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Couleur des flux positifs" ]]
                , div [Html.Attributes.class "color-picker"]
                      [ input [ type_ "color"
                              , onInput <| SetChartColor "positive bar"
                              , value model.waterfallChart.positiveBarColor
                              ][]
                      ]
                ]
            , span [Html.Attributes.class "separation-barre", waterFallStyle] []
            , div [ Html.Attributes.class "two-col" , waterFallStyle] ---------- negative color
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Couleur des flux négatifs" ]]
                , div [Html.Attributes.class "color-picker"]
                      [ input [ type_ "color"
                              , onInput <| SetChartColor "negative bar"
                              , value model.waterfallChart.negativeBarColor
                              ][]
                      ]
                ]
            , span [Html.Attributes.class "separation-barre", waterFallStyle] []
            , div [ Html.Attributes.class "two-col" , waterFallStyle] ---------- line color
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Couleur des traits" ]]
                , div [Html.Attributes.class "color-picker"]
                      [ input [ type_ "color"
                              , onInput <| SetChartColor "line"
                              , value model.waterfallChart.lineColor
                              ][]
                      ]
                ]

            ]

      ]


titleSettingsView : ChartSettingsModel -> Html Msg
titleSettingsView settings =
  let
    disabledEditTitle = not settings.showTitle
    disabledEditSource = not settings.showSource

    titleIsEmpty = String.isEmpty (String.replace " " "" settings.title)
    sourceIsEmpty = String.isEmpty (String.replace " " "" settings.source)

    displayAttrTitle = Html.Attributes.style "display" <| if disabledEditTitle then "none" else "flex"
    displayAttrSource = Html.Attributes.style "display" <| if disabledEditSource then "none" else "flex"
  in
  div []
      [ div [Html.Attributes.class "title"]
            [ img [src "../assets/icones-svg/settings/fleche-icone-1.svg"][]
            , h3 [] [Html.text "Titre"]
            ]
      , div [ Html.Attributes.class "style-options"
            ]
            [ div [ Html.Attributes.class "two-col" ] ---------- show title checkbox
                  [ div [Html.Attributes.class "option-name"]
                        [ span [] [ Html.text "Afficher le titre" ]]
                  , div [Html.Attributes.class "checkbox"]
                        [ input [ type_ "checkbox"
                                , id "showTitle"
                                , onCheck SetTitleVisibility
                                , checked settings.showTitle
                                ] []
                        , label [for "showTitle"][]
                        ]
                  ]
            -- , span [Html.Attributes.class "separation-barre"] []
            , div [ Html.Attributes.class "four-col", displayAttrTitle] ---------- txt settings for title
                    [ div [] -- title input
                          [ input [ type_ "text"
                          , placeholder "Titre"
                          , onInput SetChartTitle ] []
                          ]
                    , div [] -- color input
                          [ div [ Html.Attributes.class "color-picker"]
                                [ input [ type_ "color"
                                        , onInput <| SetTxtAttributes "title" "fontColor"
                                        , value settings.titleTxtSettings.fontColor][]
                                ]
                          ]
                    , div [] -- position input
                         [ select [ onChange SetTitlePos, disabled titleIsEmpty ]
                                  [ option [ value "top_left" ] [ Html.text "top left" ]
                                  , option [ value "top_center" ] [ Html.text "top center" ]
                                  , option [ value "top_right" ] [ Html.text "top right" ]
                                  , option [ value "bottom_left" ] [ Html.text "bottom left" ]
                                  , option [ value "bottom_center" ] [ Html.text "bottom center" ]
                                  , option [ value "bottom_right" ] [ Html.text "bottom right" ]
                                  ]
                         ]
                    , div [] -- input font size
                         [ select [ onChange <| SetTxtAttributes  "title" "fontSize"]
                                  ( List.map (fontSizeOptions settings "title") <| List.map String.fromInt <|List.range 8 30 )
                         ]
                    ]
            -- , span [Html.Attributes.class "separation-barre"] []
            , div [ Html.Attributes.class "two-col" ] ---------- show source checkbox
                  [ div [Html.Attributes.class "option-name"]
                        [ span [] [ Html.text "Afficher la source" ]]
                  , div [Html.Attributes.class "checkbox"]
                        [ input [ type_ "checkbox"
                                , id "showSource"
                                , onCheck SetSourceVisibility
                                , checked settings.showSource
                                ] []
                        , label [for "showSource"][]
                        ]
                  ]
            -- , span [Html.Attributes.class "separation-barre", Html.Attributes.style "display" <| if disabledEditSource then "none" else "flex"] []
            , div [ Html.Attributes.class "four-col", displayAttrSource] ----------  txt settings for source
                  [ div [] -- title input
                        [ input [ type_ "text"
                        , placeholder "Source"
                        , onInput SetChartSource ] []
                        ]
                  , div [] -- color input
                        [ div [Html.Attributes.class "color-picker"]
                              [ input [ type_ "color"
                                      , onInput <| SetTxtAttributes "source" "fontColor"
                                      , value settings.sourceTxtSettings.fontColor][]
                              ]
                        ]
                  , div [] -- position input
                       [ select [ onChange SetSourcePos, disabled sourceIsEmpty]
                                [ option [ value "top_left" ] [ Html.text "top left" ]
                                , option [ value "top_center" ] [ Html.text "top center" ]
                                , option [ value "top_right" ] [ Html.text "top right" ]
                                , option [ value "bottom_left" ] [ Html.text "bottom left" ]
                                , option [ value "bottom_center" ] [ Html.text "bottom center" ]
                                , option [ value "bottom_right" ] [ Html.text "bottom right" ]
                                ]
                       ]
                  , div [] -- input font size
                       [ select [ onChange <| SetTxtAttributes  "source" "fontSize"]
                                ( List.map (fontSizeOptions settings "source") <| List.map String.fromInt <|List.range 8 30 )
                       ]
                  ]
            ]


      ]


axeSettingsView : ChartSettingsModel -> Html Msg
axeSettingsView settings =
  let
    disabledEditAxes = not settings.showAxes
    displayAttr = Html.Attributes.style "display" <| if disabledEditAxes then "none" else "flex"
  in
  div [ ]
      [ div [Html.Attributes.class "title"]
            [ img [src "../assets/icones-svg/settings/fleche-icone-1.svg"][]
            , h3 [][Html.text "Axes"]
            ]
      , div [ Html.Attributes.class "style-options"
            ]
            [ div [ Html.Attributes.class "two-col" ] ---------- show axes checkbox
                  [ div [Html.Attributes.class "option-name"]
                        [ span [] [ Html.text "Afficher les axes" ]]
                  , div [Html.Attributes.class "checkbox"]
                        [ input [ type_ "checkbox"
                                , id "showAxes"
                                , onCheck SetAxesVisibility
                                , checked settings.showAxes
                                ] []
                        , label [for "showAxes"][]
                        ]
                  ]
            -- , span [Html.Attributes.class "separation-barre",  displayAttr] []
            , div [ Html.Attributes.class "four-col" ,displayAttr] ---------- axe X txt settings
                    [ div [] -- title input
                          [ input [ type_ "text"
                          , placeholder "Titre de l'axe X"
                          , disabled disabledEditAxes
                          , onInput SetAxeX ] []
                          ]
                    , div [] -- color input
                          [ div [Html.Attributes.class "color-picker"]
                                [ input [ type_ "color"
                                        , onInput <| SetTxtAttributes "axe-x" "fontColor"
                                        , disabled disabledEditAxes
                                        , value settings.axeXTxtSettings.fontColor][]
                                ]
                          ]
                    , div [] -- police input
                         [ select [ onChange <| SetTxtAttributes "axe-x" "fontFamily", disabled disabledEditAxes]
                                  ( List.map ( fontFamilyOptions settings "axe-x") <| List.sort fontFamilyList)
                         ]
                    , div [] -- input font size
                         [ select [ onChange <| SetTxtAttributes "axe-x" "fontSize", disabled disabledEditAxes ]
                                  ( List.map (fontSizeOptions settings "axe-x") <| List.map String.fromInt <|List.range 8 30)
                         ]
                    ]
            -- , span [Html.Attributes.class "separation-barre",  displayAttr] []
            , div [ Html.Attributes.class "four-col", displayAttr] ---------- axe Y txt attributes
                    [ div [] -- title
                          [ input [ type_ "text"
                          , placeholder "Titre de l'axe Y"
                          , disabled disabledEditAxes
                          , onInput SetAxeY ] []
                          ]
                    , div [] -- color
                          [ div [Html.Attributes.class "color-picker"]
                                [ input [ type_ "color"
                                        , onInput <| SetTxtAttributes "axe-y" "fontColor"
                                        , disabled disabledEditAxes
                                        , value settings.axeYTxtSettings.fontColor][]
                                ]
                          ]
                    , div [] -- police
                         [ select [ onChange <| SetTxtAttributes "axe-y" "fontFamily", disabled disabledEditAxes]
                                  ( List.map ( fontFamilyOptions settings "axe-y") <| List.sort fontFamilyList)
                         ]
                    , div [] -- font size
                         [ select [ onChange <| SetTxtAttributes "axe-y" "fontSize" , disabled disabledEditAxes]
                                  ( List.map (fontSizeOptions settings "axe-y") <| List.map String.fromInt <|List.range 8 30)
                         ]
                    ]
            ]

      ]


dataSettingsView : ChartSettingsModel -> Html Msg
dataSettingsView settings =
  let
    disabledEditValue = not settings.showValueOnHover
    displayAttr = Html.Attributes.style "display" <| if disabledEditValue then "none" else "flex"
    displayAttr2 = Html.Attributes.style "display" <| if disabledEditValue then "none" else "grid"
  in
  div []
      [ div [Html.Attributes.class "title"]
            [ img [src "../assets/icones-svg/settings/fleche-icone-1.svg"][]
            , h3 [][Html.text "Valeurs"]
            ]
      , div [ Html.Attributes.class "style-options" ]
            [ div [ Html.Attributes.class "two-col" ] -- show value on mousehover
                  [ div [Html.Attributes.class "option-name"]
                        [ span [] [ Html.text "Afficher les valeurs au survol" ]]
                  , div [Html.Attributes.class "checkbox"]
                        [
                          input [ type_ "checkbox"
                                , id "showVal"
                                , onCheck SetValueVisibility
                                , checked settings.showValueOnHover
                                ] []
                        , label [for "showVal"][]
                        ]
                  ]
            , span [Html.Attributes.class "separation-barre", displayAttr] []
            , div [ Html.Attributes.class "two-col", displayAttr] -- simple data
                  [ div [Html.Attributes.class "option-name"]
                        [ span [] [ Html.text "Données simplifiées" ]]
                  , div [Html.Attributes.class "checkbox"]
                        [
                          input [ type_ "checkbox"
                                , id "shortData"
                                , onCheck SetSimpleData
                                , checked settings.simpleData
                                , disabled disabledEditValue
                                ] []
                        , label [for "shortData"][]
                        ]
                  ]
            , span [Html.Attributes.class "separation-barre", displayAttr] []
            , div [ Html.Attributes.class "four-col-type2", displayAttr2] ---------- format data
                    [ div [] -- Séparateur décimal
                          [ div [] [ span [] [Html.text "Séparateur décimal"] ]
                          , div []
                                [ select [ onChange SetDecimalSeparator, disabled <| settings.simpleData || disabledEditValue]
                                          [ option [value ".", selected (settings.decimalSeparator == ".")][Html.text "."]
                                          , option [value ",", selected (settings.decimalSeparator == ",")] [Html.text ","]

                                          ]
                                ]
                          ]
                    , div [] -- Séparateur de milliers
                          [ div [] [ span [] [Html.text "Séparateur de milliers"] ]
                          , div []
                                [ select [ onChange SetThousandsSeparator, disabled <| settings.simpleData || disabledEditValue]
                                          [ option [value ",", selected (settings.thousandsSeparator == ",") ][Html.text ","]
                                          , option [value ".", selected (settings.thousandsSeparator == ".")] [Html.text "."]
                                          , option [value " ", selected (settings.thousandsSeparator == " ")] [Html.text "espace"]
                                          ]
                                ]
                          ]
                    ]
            , span [Html.Attributes.class "separation-barre", displayAttr] []
            , div [ Html.Attributes.class "four-col-type2", displayAttr2] ---------- prefixe suffixe
                    [ div [] -- prefixe
                          [ div [] [ span [] [Html.text "Préfixe"] ]
                          , div []
                                [ input [ type_ "text"
                                        , placeholder "$"
                                        , onInput SetDataPrefix
                                        , value settings.dataPrefix
                                        , disabled disabledEditValue
                                        ][]
                                ]
                          ]
                    , div [] -- suffixe
                          [ div [] [ span [] [Html.text "Suffixe"] ]
                          , div []
                                [ input [ type_ "text"
                                        , placeholder "%"
                                        , onInput SetDataSuffix
                                        , value settings.dataSuffix
                                        , disabled disabledEditValue
                                        ][]
                                ]
                          ]
                    ]
            , span [Html.Attributes.class "separation-barre", displayAttr] []
            , div [ Html.Attributes.class "three-col", displayAttr] -- text style
                  [ div [] -- color
                        [ div [Html.Attributes.class "color-picker"]
                              [ input [ type_ "color"
                                      , onInput <| SetTxtAttributes "data-value" "fontColor"
                                      , disabled disabledEditValue
                                      , value settings.valueTxtSettings.fontColor][]
                              ]
                        ]
                  , div [] -- police
                       [ select [ onChange <| SetTxtAttributes "data-value" "fontFamily", disabled disabledEditValue ]
                                ( List.map (fontFamilyOptions settings "data-value") <| List.sort fontFamilyList)
                       ]
                  , div [] -- input font size
                       [ select [ onChange <| SetTxtAttributes "data-value" "fontSize", disabled disabledEditValue ]
                                ( List.map (fontSizeOptions settings "data-value") <| List.map String.fromInt <|List.range 8 30)
                       ]
                  ]
            ]


      ]


legendSettingsView : ChartSettingsModel -> Html Msg
legendSettingsView settings =
  let
    disabledEditLegend = not settings.showLegend

    displayAttr = Html.Attributes.style "display" <| if disabledEditLegend then "none" else "flex"
  in
  div []
      [ div [Html.Attributes.class "title"]
            [ img [src "../assets/icones-svg/settings/fleche-icone-1.svg" ][]
            , h3 [][Html.text "Légende"]
            ]
      , div [ Html.Attributes.class "style-options" ]
            [ div [Html.Attributes.class "two-col"] -- show legend checkbox
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Afficher la légende" ]]
                , div [Html.Attributes.class "checkbox"]
                      [
                        input [ type_ "checkbox"
                              , id "showLegend"
                              , onCheck SetShowLegend
                              , checked settings.showLegend] []
                      , label [for "showLegend"][]
                      ]

                ]
            , span [Html.Attributes.class "separation-barre", displayAttr] []
            , div [Html.Attributes.class "two-col", displayAttr] -- position
                [ div [Html.Attributes.class "option-name"]
                      [ span [] [ Html.text "Position de la légende" ]
                      ]
                , div []
                      [ select [ onChange SetLegendPos, disabled disabledEditLegend ]
                               [ option [ value "top_left" ] [ Html.text "top left" ]
                               , option [ value "top_center" ] [ Html.text "top center" ]
                               , option [ value "top_right" ] [ Html.text "top right" ]
                               , option [ value "middle_left" ] [ Html.text "middle left" ]
                               , option [ value "middle_right" ] [ Html.text "middle right" ]
                               , option [ value "bottom_left" ] [ Html.text "bottom left" ]
                               , option [ value "bottom_center", selected True ] [ Html.text "bottom center" ]
                               , option [ value "bottom_right" ] [ Html.text "bottom right" ]
                               ]
                      ]
                ]
            , span [Html.Attributes.class "separation-barre", displayAttr] []
            , div [ Html.Attributes.class "two-col", displayAttr] -- auto color checkbox
                  [ div [Html.Attributes.class "option-name"]
                        [ span [] [ Html.text "Couleur automatique" ]]
                  , div [Html.Attributes.class "checkbox"]
                        [
                          input [ type_ "checkbox"
                                , id "autoColor"
                                , disabled disabledEditLegend
                                , onCheck SetAutoColorForLegend
                                , checked settings.legendTxtSettings.autoColor] []
                        , label [for "autoColor"][]
                        ]
                  ]
            , span [Html.Attributes.class "separation-barre", displayAttr] []
            , div [ Html.Attributes.class "three-col", displayAttr] -- legend txt attributes
                    [ div [] -- color
                          [ div [Html.Attributes.class "color-picker"]
                                [ input [ type_ "color"
                                        , onInput <| SetTxtAttributes "legend" "fontColor"
                                        , disabled disabledEditLegend
                                        , value settings.legendTxtSettings.fontColor][]
                                ]
                          ]
                    , div [] -- police
                         [ select [ onChange <| SetTxtAttributes "legend" "fontFamily" , disabled disabledEditLegend]
                                  ( List.map (fontFamilyOptions settings "legend") <| List.sort fontFamilyList)
                         ]
                    , div [] -- font size
                         [ select [ onChange <| SetTxtAttributes "legend" "fontSize" , disabled disabledEditLegend]
                                  ( List.map (fontSizeOptions settings "legend") <| List.map String.fromInt <|List.range 8 30)
                         ]
                    ]
            ]

      ]




--------------- others

fontFamilyOptions : ChartSettingsModel -> String -> String  -> Html Msg
fontFamilyOptions model field fontFamily  =
  let
    settingFontFamily =
      case field of
        "title" -> model.titleTxtSettings.fontFamily
        "source" -> model.sourceTxtSettings.fontFamily
        "axe-x" -> model.axeXTxtSettings.fontFamily
        "axe-y" -> model.axeYTxtSettings.fontFamily
        "data-value" -> model.valueTxtSettings.fontFamily
        "legend" -> model.legendTxtSettings.fontFamily
        _ -> model.globalFontFamily
  in
  option [ value <| if fontFamily == "-- Police --" then "none" else fontFamily
         , selected (if fontFamily == settingFontFamily then True else False)
         ]
         [ Html.text fontFamily ]


fontSizeOptions : ChartSettingsModel -> String -> String -> Html Msg
fontSizeOptions model field fontSize =
  let
    settingFontSize =
      case field of
        "title" -> model.titleTxtSettings.fontSize
        "source" -> model.sourceTxtSettings.fontSize
        "axe-x" -> model.axeXTxtSettings.fontSize
        "axe-y" -> model.axeYTxtSettings.fontSize
        "data-value" -> model.valueTxtSettings.fontSize
        "legend" -> model.legendTxtSettings.fontSize
        _ -> model.titleTxtSettings.fontSize
  in
  option [ value fontSize
         , selected <| (String.fromInt settingFontSize) == fontSize
         ]
         [ Html.text fontSize ]
