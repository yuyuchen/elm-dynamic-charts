# ELM Dynamic Charts 

Ce projet a été réalisé dans le cadre d'un stage chez Prezance en 2020, et mise en open source. 

Le but est de créer une librairie de graphe dynamique en ELM, 100% personalisable.


## Pré-requis
* installer `elm`
* un editeur de text (Atom, Visual Studio Code,..)


## Lancer le projet
Deux solutions possibles pour lançer le projet : 


* Dans `src`, ouvrez `index.html` avec votre navigateur

OU 

* Ouvrez une console dans le dossier `elm-dynamic-charts-prezance`
* tapez la commande suivant : `elm reactor`
* ouvrez votre navigateur et tapez `http://localhost:8000`, vous devez avoir une page qui affiche les dossiers et fichiers du project
* toujours sur le port `8000`, cliquez sur `src` puis `Main.elm`
* Vous avez maintenant la page afficher


## Démo 
Vous pouvez trouvez une démo du projet via ce [lien](https://drive.google.com/file/d/1A_qEzjBg0_Hmqg6qG5c4qUIUzyQqiP5D/view?usp=sharing)
